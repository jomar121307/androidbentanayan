package com.zoogtech.onlineshopping;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.modules.cart.CartShopsFragment;
import com.zoogtech.onlineshopping.modules.history.PurchaseHistoryOrdersFragment;
import com.zoogtech.onlineshopping.modules.home.HomeFragment;
import com.zoogtech.onlineshopping.modules.login.LoginMainFragment;
import com.zoogtech.onlineshopping.modules.messages.DirectMessagesFragment;
import com.zoogtech.onlineshopping.modules.shop.ShopMainFragment;
import com.zoogtech.onlineshopping.utils.Cache;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, FragmentManager.OnBackStackChangedListener {
    ActionBarDrawerToggle toggle;
    FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        printKeyHash(this);

        fm = getSupportFragmentManager();
        fm.addOnBackStackChangedListener(this);

        //  Set up toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //  Set up nav drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (isLoggedIn()) {
                    TextView user_name = ButterKnife.findById(BaseActivity.this, R.id.user_name);
                    TextView user_email = ButterKnife.findById(BaseActivity.this, R.id.user_email);
                    CircleImageView user_avatar = ButterKnife.findById(BaseActivity.this, R.id.user_avatar);
                    Session session = null;
                    try {
                        session = (Session) Cache.readObject(BaseActivity.this, "session");
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    if (session != null) {
                        user_name.setText(session.getUserId().getFirstName() + " " + session.getUserId().getLastName());
                        user_email.setText(session.getUserId().getEmail());
                        if (session.getUserId().getProfilePhoto().getSecure_url() != null) {
                            Glide.with(BaseActivity.this)
                                    .load(session.getUserId().getProfilePhoto().getSecure_url())
                                    .fitCenter()
                                    .into(user_avatar);
                        } else {
                            Glide.with(BaseActivity.this)
                                    .load(R.drawable.ph_user)
                                    .fitCenter()
                                    .into(user_avatar);
                        }
                    } else {
                        user_name.setText("Guest User");
                        user_email.setText("Login for full app usage");
                        Glide.with(BaseActivity.this)
                                .load(R.drawable.ph_user)
                                .fitCenter()
                                .into(user_avatar);
                    }
                } else {
                    TextView user_name = ButterKnife.findById(BaseActivity.this, R.id.user_name);
                    TextView user_email = ButterKnife.findById(BaseActivity.this, R.id.user_email);
                    CircleImageView user_avatar = ButterKnife.findById(BaseActivity.this, R.id.user_avatar);
                    user_name.setText("Guest User");
                    user_email.setText("Login for full app usage");
                    Glide.with(BaseActivity.this)
                            .load(R.drawable.ph_user)
                            .fitCenter()
                            .into(user_avatar);
                }
                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                navigationView.getMenu().getItem(6).setVisible(false);
                navigationView.getMenu().getItem(7).setVisible(false);
                if (isLoggedIn()) {
                    navigationView.getMenu().getItem(0).setVisible(false);
                    navigationView.getMenu().getItem(2).setVisible(true);
                    navigationView.getMenu().getItem(3).setVisible(true);
                    navigationView.getMenu().getItem(4).setVisible(true);
                    navigationView.getMenu().getItem(5).setVisible(true);
                    navigationView.getMenu().getItem(8).setVisible(true);
                } else {
                    navigationView.getMenu().getItem(0).setVisible(true);
                    navigationView.getMenu().getItem(2).setVisible(false);
                    navigationView.getMenu().getItem(3).setVisible(false);
                    navigationView.getMenu().getItem(4).setVisible(false);
                    navigationView.getMenu().getItem(5).setVisible(false);
                    navigationView.getMenu().getItem(8).setVisible(false);
                }
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                String title = toolbar.getTitle().toString();
                switch (title) {
                    case "Login":
                    case "Register":
                        navigationView.getMenu().getItem(0).setChecked(true);
                        break;
                    case "Home":
                        navigationView.getMenu().getItem(1).setChecked(true);
                        break;
                    case "My Shop":
                        navigationView.getMenu().getItem(2).setChecked(true);
                        break;
                    case "Cart - Select Shop":
                        navigationView.getMenu().getItem(3).setChecked(true);
                        break;
                    case "Purchased Items":
                        navigationView.getMenu().getItem(4).setChecked(true);
                        break;
                    case "Messages":
                        navigationView.getMenu().getItem(5).setChecked(true);
                        break;
                }
            }
        };
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        drawer.setDrawerListener(toggle);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().getItem(1).setChecked(true);

        HomeFragment fragment = new HomeFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        android.support.v4.app.FragmentTransaction ft;
        switch(item.getItemId()) {
            case R.id.nav_login:
                if (!toolbar.getTitle().equals("Login") && !toolbar.getTitle().equals("Register")) {
                    LoginMainFragment lf = new LoginMainFragment();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.frame, lf);
                    ft.commit();
                }
                break;
            case R.id.nav_buy:
                if (!toolbar.getTitle().equals("Home")) {
                    HomeFragment hf = new HomeFragment();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.frame, hf);
                    ft.commit();
                }
                break;
            case R.id.nav_shop:
                if (!toolbar.getTitle().equals("My Shop")) {
                    ShopMainFragment hf = new ShopMainFragment();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.frame, hf);
                    ft.commit();
                }
                break;
            case R.id.nav_cart:
                if (!toolbar.getTitle().equals("Cart - Select Shop")) {
                    CartShopsFragment hf = new CartShopsFragment();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.frame, hf);
                    ft.commit();
                }
                break;
            case R.id.nav_purchase_history:
                if (!toolbar.getTitle().equals("Purchased Items")) {
                    PurchaseHistoryOrdersFragment phf = new PurchaseHistoryOrdersFragment();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.frame, phf);
                    ft.commit();
                }
                break;
            case R.id.nav_dm:
                if (!toolbar.getTitle().equals("Messages")) {
                    DirectMessagesFragment dmf = new DirectMessagesFragment();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.frame, dmf);
                    ft.commit();
                }
                break;
            case R.id.nav_notifications:
                break;
            case R.id.nav_account:
                break;
            case R.id.nav_logout:
                try {
                    if (Cache.deleteObject(getApplicationContext(), "session")) {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                        LoginMainFragment lf2 = new LoginMainFragment();
                        ft = fm.beginTransaction();
                        ft.replace(R.id.frame, lf2);
                        ft.commit();
                        OnlineShopping.clearData();
                    } else {
                        Toast.makeText(getApplicationContext(), "Error logging out. Please try again.", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

//    public void switchContent(Fragment fragment, String tag) {
//        final int newBackStackLength = getSupportFragmentManager().getBackStackEntryCount() +1;
//        final FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.frame, fragment, tag)
//                .addToBackStack(tag)
//                .commit();
//
//        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
//            @Override
//            public void onBackStackChanged() {
//                int nowCount = fragmentManager.getBackStackEntryCount();
//                if (newBackStackLength != nowCount) {
//                    // we don't really care if going back or forward. we already performed the logic here.
//                    fragmentManager.removeOnBackStackChangedListener(this);
//
//                    if ( newBackStackLength > nowCount ) { // user pressed back
//                        fragmentManager.popBackStackImmediate();
//                    }
//                }
//            }
//        });
//    }

    public void switchContent(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame, fragment, fragment.toString());
        ft.addToBackStack(null);
        ft.commit();
    }

    public void manualBack() {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackStackChanged() {
        toggle.setDrawerIndicatorEnabled(fm.getBackStackEntryCount() == 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(fm.getBackStackEntryCount() > 0);
        toggle.syncState();
    }

    public boolean isLoggedIn() {
        boolean toReturn;
        toReturn = OnlineShopping.getSession() != null;
        return toReturn;
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }
}
