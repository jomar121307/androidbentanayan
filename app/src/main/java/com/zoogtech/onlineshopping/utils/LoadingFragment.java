package com.zoogtech.onlineshopping.utils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.LoginResponse;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.modules.home.HomeFragment;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 3/16/16.
 */
public class LoadingFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_loading, container, false);

        Bundle b = getArguments();

        final TextView loading_message = (TextView) v.findViewById(R.id.loading_message);
        if (b.getString("token") != null) {
            loading_message.setText("Attempting to log in");
            ApiService service = new RestClient().getApiService();
            Call<LoginResponse> call = service.loginWithFb(b.getString("token"), "fb");
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    Session s = response.body().getSession();
                    s.setShopId(response.body().getShop());
                    OnlineShopping.setSession(s);
                    OnlineShopping.setUser(s.getUserId());
                    try {
                        Cache.writeObject(getContext(), "session", s);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    HomeFragment fragment = new HomeFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame, fragment);
                    fragmentTransaction.commit();
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    loading_message.setText("There was an error. Please try again.");
                }
            });
        }

        return v;
    }
}
