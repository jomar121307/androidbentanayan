package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by rnecesito on 3/17/16.
 */
public class Currency implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code;
    private String currency;
    private String symbol;

    public String getCode() {
        return code;
    }

    public String getCurrency() {
        return currency;
    }

    public String getSymbol() {
        return symbol;
    }
}
