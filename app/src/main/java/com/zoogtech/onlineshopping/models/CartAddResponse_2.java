package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by rnecesito on 3/23/16.
 */
public class CartAddResponse_2 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String userId;
    private String shopId;
    private Product_3 productId;
    private int quantity;
    private String createdAt;
    private String updatedAt;
    private String id;

    public String getUserId() {
        return userId;
    }

    public String getShopId() {
        return shopId;
    }

    public Product_3 getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getId() {
        return id;
    }
}
