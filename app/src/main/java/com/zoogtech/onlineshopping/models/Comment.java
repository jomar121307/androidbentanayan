package com.zoogtech.onlineshopping.models;

/**
 * Created by Rey on 1/25/2016.
 */
public class Comment {
    private static final long serialVersionUID = 1L;
    private String content;
    private String createdAt;
    private String id;
    private boolean isDeleted;
    private boolean isReply;
    private String postId;
    private String updatedAt;
    private User userId;
}
