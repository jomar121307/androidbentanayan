package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by Rey on 1/26/2016.
 */
public class LoginResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private Session session;
    private String shop;

    public Session getSession() {
        return session;
    }

    public String getShop() {
        return shop;
    }
}
