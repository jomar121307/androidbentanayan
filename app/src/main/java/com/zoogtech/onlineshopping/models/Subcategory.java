package com.zoogtech.onlineshopping.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by rnecesito on 3/16/16.
 */
public class Subcategory implements Serializable {
    private static final long serialVersionUID = 1L;
    private Group groupheader;
    private ArrayList<Group> groupling;

    public Group getGroupheader() {
        return groupheader;
    }

    public ArrayList<Group> getGroupling() {
        return groupling;
    }

    public class Group implements Serializable {
        private static final long serialVersionUID = 1L;
        private String name;
        private String slug;

        public String getName() {
            return name;
        }

        public String getSlug() {
            return slug;
        }
    }
}
