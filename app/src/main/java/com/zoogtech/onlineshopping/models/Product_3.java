package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by rnecesito on 3/17/16.
 */
public class Product_3 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String userId;
    private String currencyId;
    private String brand;
    private String name;
    private String description;
    private String category;
    private int quantity;
    private float price;
    private Metadata metadata;
    private float reviewScore;
    private int reviewCount;
    private String createdAt;
    private String updatedAt;
    private String id;

    public String getUserId() {
        return userId;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public String getBrand() {
        return brand;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }

    public int getQuantity() {
        return quantity;
    }

    public float getPrice() {
        return price;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public float getReviewScore() {
        return reviewScore;
    }

    public int getReviewCount() {
        return reviewCount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getId() {
        return id;
    }
}
