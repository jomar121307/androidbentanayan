package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by Rey on 1/26/2016.
 */
public class Session implements Serializable {
    private static final long serialVersionUID = 1L;
    private User userId;
    private String accessToken;
    private String refreshToken;
    private int expiresAt;
    private String createdAt;
    private String id;
    private String shopId;

    public User getUserId() {
        return userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public int getExpiresAt() {
        return expiresAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getId() {
        return id;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
