package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by Rey on 1/18/2016.
 */
public class Page implements Serializable {
    private User ownerId;
    private String name;
    private String title;
    private String description;
    private MediaMetadata pagePhoto;
    private MediaMetadata coverPhoto;
    private int visitCount;
    private int followerCount;
    private boolean isFollowed;
    private String id;

    public Page(User ownerId, String name, String title, String description, MediaMetadata pagePhoto, MediaMetadata coverPhoto, int visitCount, int followerCount, boolean isFollowed) {
        this.ownerId = ownerId;
        this.name = name;
        this.title = title;
        this.description = description;
        this.pagePhoto = pagePhoto;
        this.coverPhoto = coverPhoto;
        this.visitCount = visitCount;
        this.followerCount = followerCount;
        this.isFollowed = isFollowed;
    }

    public User getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(User ownerId) {
        this.ownerId = ownerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MediaMetadata getPagePhoto() {
        return pagePhoto;
    }

    public void setPagePhoto(MediaMetadata pagePhoto) {
        this.pagePhoto = pagePhoto;
    }

    public MediaMetadata getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(MediaMetadata coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public int getVisitCount() {
        return visitCount;
    }

    public void setVisitCount(int visitCount) {
        this.visitCount = visitCount;
    }

    public int getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(int followerCount) {
        this.followerCount = followerCount;
    }

    public boolean getIsFollowed() {
        return isFollowed;
    }

    public void setIsFollowed(boolean isFollowed) {
        this.isFollowed = isFollowed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
