package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by rnecesito on 3/30/16.
 */
public class Message implements Serializable {
    private static final long serialVersionUID = 1L;
    private String roomId;
    private String to;
    private String from;
    private Payload payload;
    private String appType;
    private String status;
    private boolean isDeleted;
    private String createdAt;
    private String updatedAt;
    private String id;

    public String getRoomId() {
        return roomId;
    }

    public String getTo() {
        return to;
    }

    public String getFrom() {
        return from;
    }

    public Payload getPayload() {
        return payload;
    }

    public String getAppType() {
        return appType;
    }

    public String getStatus() {
        return status;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getId() {
        return id;
    }

    public class Payload implements Serializable {
        private static final long serialVersionUID = 1L;
        private String message;

        public String getMessage() {
            return message;
        }
    }
}
