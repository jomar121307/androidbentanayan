package com.zoogtech.onlineshopping.models;

import java.util.ArrayList;

/**
 * Created by rnecesito on 3/17/16.
 */
public class ProductsResponse {
    private static final long serialVersionUID = 1L;
    private int count;
    private ArrayList<Product> products;

    public int getCount() {
        return count;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }
}
