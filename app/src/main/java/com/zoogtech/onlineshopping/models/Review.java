package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by rnecesito on 4/2/16.
 */
public class Review implements Serializable {
    private static final long serialVersionUID = 1L;
    private String productId;
    private User userId;
    private String content;
    private boolean isReply;
    private float rating;
    private Metadata metadata;
    private String createdAt;
    private String updatedAt;
    private String id;

    public Review(String productId, User userId, String content, boolean isReply, float rating, Metadata metadata, String createdAt, String updatedAt, String id) {
        this.productId = productId;
        this.userId = userId;
        this.content = content;
        this.isReply = isReply;
        this.rating = rating;
        this.metadata = metadata;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public boolean isReply() {
        return isReply;
    }

    public float getRating() {
        return rating;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getId() {
        return id;
    }

    public class Metadata implements Serializable{
        private static final long serialVersionUID = 1L;
        private int likeCount;

        public int getLikeCount() {
            return likeCount;
        }
    }
}
