package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by Rey on 1/25/2016.
 */
public class CheckoutInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String cardId;
    private String currencyCode;
    private String shippingId;

    public CheckoutInfo(String cardId, String currencyCode, String shippingId) {
        this.cardId = cardId;
        this.currencyCode = currencyCode;
        this.shippingId = shippingId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getShippingId() {
        return shippingId;
    }

    public void setShippingId(String shippingId) {
        this.shippingId = shippingId;
    }
}
