package com.zoogtech.onlineshopping.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Rey on 1/25/2016.
 */
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private String avatar;
    private String birthday;
    private CheckoutInfo checkoutInfo;
    private String city;
    private String country;
    private String createdAt;
    private DefaultCurrency defaultCurrency;
    private String email;
    private String firstName;
    private String gender;
    private String id;
    private String lastName;
    private String middleName;
    private int notificationCount;
    private int pageCount;
    private String stateProvince;
    private String updatedAt;
    private ProfilePhoto profilePhoto;
    private ArrayList<String> paymentPlatformsAvailable;

    public String getAvatar() {
        return avatar;
    }

    public String getBirthday() {
        return birthday;
    }

    public CheckoutInfo getCheckoutInfo() {
        return checkoutInfo;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public DefaultCurrency getDefaultCurrency() {
        return defaultCurrency;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getGender() {
        return gender;
    }

    public String getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public int getNotificationCount() {
        return notificationCount;
    }

    public int getPageCount() {
        return pageCount;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public ProfilePhoto getProfilePhoto() {
        return profilePhoto;
    }

    public ArrayList<String> getPaymentPlatformsAvailable() {
        return paymentPlatformsAvailable;
    }

    public class ProfilePhoto implements Serializable {
        private static final long serialVersionUID = 1L;
        private boolean is_silhouette;
        private String secure_url;
        private String source;

        public boolean is_silhouette() {
            return is_silhouette;
        }

        public String getSecure_url() {
            return secure_url;
        }

        public String getSource() {
            return source;
        }
    }
}
