package com.zoogtech.onlineshopping.models;

import java.util.ArrayList;

/**
 * Created by rnecesito on 3/18/16.
 */
public class CartResponse {
    private static final long serialVersionUID = 1L;
    private int count;
    private ArrayList<CartItem> items;

    public int getCount() {
        return count;
    }

    public ArrayList<CartItem> getItems() {
        return items;
    }
}
