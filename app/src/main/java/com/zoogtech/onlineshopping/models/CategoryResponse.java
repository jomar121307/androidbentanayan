package com.zoogtech.onlineshopping.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by rnecesito on 3/31/16.
 */
public class CategoryResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private ArrayList<Category> metadata;

    public String getName() {
        return name;
    }

    public ArrayList<Category> getMetadata() {
        return metadata;
    }
}
