package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by rnecesito on 5/27/16.
 */
public class ResetPassResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String status;
    private String msg;

    public String getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }
}
