package com.zoogtech.onlineshopping.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Rey on 1/25/2016.
 */
public class Post implements Serializable {
    private int commentCount;
    private ArrayList<Comment> comments;
    private String content;
    private String createdAt;
    private String id;
    private boolean isDeleted;
    private boolean isLiked;
    private int likeCount;
    private MediaMetadata mediaMetadata;
    private Page_2 pageId;
    private int pagePostCount;
//    private ArrayList<String> products;
    private int reportCount;
    private String type;
    private String updatedAt;
    private User userId;

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setIsLiked(boolean isLiked) {
        this.isLiked = isLiked;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public MediaMetadata getMediaMetadata() {
        return mediaMetadata;
    }

    public void setMediaMetadata(MediaMetadata mediaMetadata) {
        this.mediaMetadata = mediaMetadata;
    }

    public Page_2 getPageId() {
        return pageId;
    }

    public void setPageId(Page_2 pageId) {
        this.pageId = pageId;
    }

    public int getPagePostCount() {
        return pagePostCount;
    }

    public void setPagePostCount(int pagePostCount) {
        this.pagePostCount = pagePostCount;
    }

    public int getReportCount() {
        return reportCount;
    }

    public void setReportCount(int reportCount) {
        this.reportCount = reportCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }
}
