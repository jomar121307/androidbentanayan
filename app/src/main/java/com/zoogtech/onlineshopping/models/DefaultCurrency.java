package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by Rey on 1/25/2016.
 */
public class DefaultCurrency implements Serializable {
    private static final long serialVersionUID = 1L;
    private String _bsontype;
    private String id;

    public DefaultCurrency(String _bsontype, String id) {
        this._bsontype = _bsontype;
        this.id = id;
    }

    public String get_bsontype() {
        return _bsontype;
    }

    public void set_bsontype(String _bsontype) {
        this._bsontype = _bsontype;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
