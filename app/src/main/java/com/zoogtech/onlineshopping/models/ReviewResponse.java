package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by rnecesito on 4/2/16.
 */
public class ReviewResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String productId;
    private String userId;
    private String content;
    private boolean isReply;
    private float rating;
    private Review.Metadata metadata;
    private String createdAt;
    private String updatedAt;
    private String id;

    public String getProductId() {
        return productId;
    }

    public String getUserId() {
        return userId;
    }

    public String getContent() {
        return content;
    }

    public boolean isReply() {
        return isReply;
    }

    public float getRating() {
        return rating;
    }

    public Review.Metadata getMetadata() {
        return metadata;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getId() {
        return id;
    }

}
