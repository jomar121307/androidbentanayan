package com.zoogtech.onlineshopping.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by rnecesito on 3/17/16.
 */
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;
    private User userId;
    private Shop shopId;
    private Currency currencyId;
    private String brand;
    private String name;
    private String description;
    private String category;
    private int quantity;
    private float price;
    private Metadata metadata;
    private float reviewScore;
    private int reviewCount;
    private String createdAt;
    private String updatedAt;
    private String id;
    private ArrayList<Review> reviews;

    public User getUserId() {
        return userId;
    }

    public Shop getShopId() {
        return shopId;
    }

    public Currency getCurrencyId() {
        return currencyId;
    }

    public String getBrand() {
        return brand;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }

    public int getQuantity() {
        return quantity;
    }

    public float getPrice() {
        return price;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public float getReviewScore() {
        return reviewScore;
    }

    public int getReviewCount() {
        return reviewCount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getId() {
        return id;
    }

    public ArrayList<Review> getReviews() {
        return reviews;
    }
}
