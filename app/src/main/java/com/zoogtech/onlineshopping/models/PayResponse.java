package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by rnecesito on 3/29/16.
 */
public class PayResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private ResponseEnvelope responseEnvelope;
    private String payKey;
    private String paymentExecStatus;
    private String httpStatusCode;
    private String paymentApprovalUrl;

    public ResponseEnvelope getResponseEnvelope() {
        return responseEnvelope;
    }

    public String getPayKey() {
        return payKey;
    }

    public String getPaymentExecStatus() {
        return paymentExecStatus;
    }

    public String getHttpStatusCode() {
        return httpStatusCode;
    }

    public String getPaymentApprovalUrl() {
        return paymentApprovalUrl;
    }

    public class ResponseEnvelope implements Serializable {
        private static final long serialVersionUID = 1L;
        private String timestamp;
        private String ack;
        private String correlationId;
        private String build;

        public String getTimestamp() {
            return timestamp;
        }

        public String getAck() {
            return ack;
        }

        public String getCorrelationId() {
            return correlationId;
        }

        public String getBuild() {
            return build;
        }
    }
}
