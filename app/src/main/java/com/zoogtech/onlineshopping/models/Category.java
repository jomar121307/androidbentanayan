package com.zoogtech.onlineshopping.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by rnecesito on 3/16/16.
 */
public class Category implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private String thumbnail;
    private ArrayList<Subcategory> subcategories;
    private String slug;

    public String getName() {
        return name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public ArrayList<Subcategory> getSubcategories() {
        return subcategories;
    }

    public String getSlug() {
        return slug;
    }
}
