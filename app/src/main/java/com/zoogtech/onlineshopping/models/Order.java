package com.zoogtech.onlineshopping.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by rnecesito on 3/29/16.
 */
public class Order implements Serializable {
    private static final long serialVersionUID = 1L;
    private User userId;
    private Shop_2 shopId;
    private Transaction transactionId;
    private Shipping_2 shippingId;
    private ArrayList<Item> items;
    private String appType;
    private String status;
    private boolean isCompleted;
    private boolean isDeleted;
    private String createdAt;
    private String updatedAt;
    private String id;

    public User getUserId() {
        return userId;
    }

    public Shop_2 getShopId() {
        return shopId;
    }

    public Transaction getTransactionId() {
        return transactionId;
    }

    public Shipping_2 getShippingId() {
        return shippingId;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public String getAppType() {
        return appType;
    }

    public String getStatus() {
        return status;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getId() {
        return id;
    }

    public class Item implements Serializable {
        private static final long serialVersionUID = 1L;
        private Product_3 productId;
        private int quantity;
        private User userId;
        private String appType;
        private Shop shopId;
        private String status;
        private boolean isDeleted;
        private String createdAt;
        private String updatedAt;
        private String id;
        private float grossAmount;
        private float netAmount;
        private float netReceive;
        private float shippingCost;

        public Product_3 getProductId() {
            return productId;
        }

        public int getQuantity() {
            return quantity;
        }

        public User getUserId() {
            return userId;
        }

        public String getAppType() {
            return appType;
        }

        public Shop getShopId() {
            return shopId;
        }

        public String getStatus() {
            return status;
        }

        public boolean isDeleted() {
            return isDeleted;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public String getId() {
            return id;
        }

        public float getGrossAmount() {
            return grossAmount;
        }

        public float getNetAmount() {
            return netAmount;
        }

        public float getNetReceive() {
            return netReceive;
        }

        public float getShippingCost() {
            return shippingCost;
        }
    }
}
