package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by rnecesito on 3/29/16.
 */
public class Transaction implements Serializable {
    private static final long serialVersionUID = 1L;
    private String userId;
    private Receivers[] receivers;
    private String payKey;
    private PaymentBreakdown paymentBreakdown;
    private String appType;
    private String status;
    private boolean isDeleted;
    private String createdAt;
    private String updatedAt;
    private String id;

    public String getUserId() {
        return userId;
    }

    public Receivers[] getReceivers() {
        return receivers;
    }

    public String getPayKey() {
        return payKey;
    }

    public PaymentBreakdown getPaymentBreakdown() {
        return paymentBreakdown;
    }

    public String getAppType() {
        return appType;
    }

    public String getStatus() {
        return status;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getId() {
        return id;
    }

    public class Receivers implements Serializable {
        private static final long serialVersionUID = 1L;
        private String shopId;
        private float amount;
        private String type;
        private String currency;
        private boolean primary;
        private float platformFee;

        public String getShopId() {
            return shopId;
        }

        public float getAmount() {
            return amount;
        }

        public String getType() {
            return type;
        }

        public String getCurrency() {
            return currency;
        }

        public boolean isPrimary() {
            return primary;
        }

        public float getPlatformFee() {
            return platformFee;
        }
    }

    public class PaymentBreakdown implements Serializable {
        private static final long serialVersionUID = 1L;
        private float tax;
        private float totalAmount;
        private float totalItemsAmount;
        private float projectedPaypalFee;
        private float handlingFee;
        private float totalShipping;
        private float platformFee;
        private String currency;
        private boolean nonDecimal;

        public float getTax() {
            return tax;
        }

        public float getTotalAmount() {
            return totalAmount;
        }

        public float getTotalItemsAmount() {
            return totalItemsAmount;
        }

        public float getProjectedPaypalFee() {
            return projectedPaypalFee;
        }

        public float getHandlingFee() {
            return handlingFee;
        }

        public float getTotalShipping() {
            return totalShipping;
        }

        public float getPlatformFee() {
            return platformFee;
        }

        public String getCurrency() {
            return currency;
        }

        public boolean isNonDecimal() {
            return nonDecimal;
        }
    }
}
