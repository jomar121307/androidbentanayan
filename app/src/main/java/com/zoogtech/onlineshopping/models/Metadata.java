package com.zoogtech.onlineshopping.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by rnecesito on 3/17/16.
 */
public class Metadata implements Serializable {
    private static final long serialVersionUID = 1L;
    private ArrayList<MediaMetadata> photos;
    private String[] tags;
    private String status;
    private ShippingData[] shippingData;
    private String productCode;

    public ArrayList<MediaMetadata> getPhotos() {
        return photos;
    }

    public String[] getTags() {
        return tags;
    }

    public String getStatus() {
        return status;
    }

    public ShippingData[] getShippingData() {
        return shippingData;
    }

    public String getProductCode() {
        return productCode;
    }

    public class ShippingData implements Serializable{
        private static final long serialVersionUID = 1L;
        private String location;
        private String singleCost;
        private String multipleCost;

        public String getLocation() {
            return location;
        }

        public String getSingleCost() {
            return singleCost;
        }

        public String getMultipleCost() {
            return multipleCost;
        }
    }
}