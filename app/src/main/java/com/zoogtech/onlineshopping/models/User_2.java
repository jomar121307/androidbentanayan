package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by Rey on 1/25/2016.
 */
public class User_2 extends User implements Serializable {
    private static final long serialVersionUID = 1L;
    private String avatar;
    private String birthday;
    private CheckoutInfo checkoutInfo;
    private String city;
    private String country;
    private String createdAt;
    private DefaultCurrency defaultCurrency;
    private String email;
    private String firstName;
    private String gender;
    private String id;
    private String lastName;
    private String middleName;
    private int notificationCount;
    private int pageCount;
    private String stateProvince;
    private String updatedAt;

    @Override
    public String getAvatar() {
        return avatar;
    }

    @Override
    public String getBirthday() {
        return birthday;
    }

    @Override
    public CheckoutInfo getCheckoutInfo() {
        return checkoutInfo;
    }

    @Override
    public String getCity() {
        return city;
    }

    @Override
    public String getCountry() {
        return country;
    }

    @Override
    public String getCreatedAt() {
        return createdAt;
    }

    @Override
    public DefaultCurrency getDefaultCurrency() {
        return defaultCurrency;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getGender() {
        return gender;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public String getMiddleName() {
        return middleName;
    }

    @Override
    public int getNotificationCount() {
        return notificationCount;
    }

    @Override
    public int getPageCount() {
        return pageCount;
    }

    @Override
    public String getStateProvince() {
        return stateProvince;
    }

    @Override
    public String getUpdatedAt() {
        return updatedAt;
    }
}
