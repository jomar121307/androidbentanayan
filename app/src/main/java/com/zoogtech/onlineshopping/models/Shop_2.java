package com.zoogtech.onlineshopping.models;

import java.io.Serializable;

/**
 * Created by rnecesito on 3/22/16.
 */
public class Shop_2 implements Serializable {
    private static final long serialVersionUID = 1L;
    private User userId;
    private User user;
    private Metadata metadata;
    private String createdAt;
    private String updatedAt;
    private String id;

    public User getUserId() {
        return userId;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getId() {
        return id;
    }

    public class Metadata implements Serializable {
        private static final long serialVersionUID = 1L;
        private String currencyId;
        private String shopName;
        private int productCount;
        private float feedbackScore;

        public String getCurrencyId() {
            return currencyId;
        }

        public String getShopName() {
            return shopName;
        }

        public int getProductCount() {
            return productCount;
        }

        public float getFeedbackScore() {
            return feedbackScore;
        }
    }
}
