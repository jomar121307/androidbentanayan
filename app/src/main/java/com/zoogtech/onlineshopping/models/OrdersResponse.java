package com.zoogtech.onlineshopping.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by rnecesito on 3/29/16.
 */
public class OrdersResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private int count;
    private ArrayList<Order> orders;

    public int getCount() {
        return count;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }
}
