package com.zoogtech.onlineshopping.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by rnecesito on 3/30/16.
 */
public class PrivateMessage implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private ArrayList<User> users;
    private String appType;
    private int messageCount;
    private boolean isDeleted;
    private int unreadCount;
    private String createdAt;
    private String updatedAt;
    private String id;
    private ArrayList<Message> messages;

    public String getName() {
        return name;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public String getAppType() {
        return appType;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getId() {
        return id;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public class User implements Serializable {
        private static final long serialVersionUID = 1L;
        private String firstName;
        private String middleName;
        private String lastName;
        private String gender;
        private ProfilePhoto profilePhoto;
        private String id;
        private boolean __self;

        public String getFirstName() {
            return firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getGender() {
            return gender;
        }

        public ProfilePhoto getProfilePhoto() {
            return profilePhoto;
        }

        public String getId() {
            return id;
        }

        public boolean is__self() {
            return __self;
        }
    }

    public class ProfilePhoto implements Serializable {
        private static final long serialVersionUID = 1L;
        private boolean is_silhouette;
        private String secure_url;
        private String source;

        public boolean is_silhouette() {
            return is_silhouette;
        }

        public String getSecure_url() {
            return secure_url;
        }

        public String getSource() {
            return source;
        }
    }
}
