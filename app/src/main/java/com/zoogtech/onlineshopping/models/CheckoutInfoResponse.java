package com.zoogtech.onlineshopping.models;

/**
 * Created by rnecesito on 3/31/16.
 */
public class CheckoutInfoResponse {
    private static final long serialVersionUID = 1L;
    private String status;

    public String getStatus() {
        return status;
    }
}
