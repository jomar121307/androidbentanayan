package com.zoogtech.onlineshopping.modules.products.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.MediaMetadata;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.trinea.android.common.util.ListUtils;

/**
 * Created by rnecesito on 3/17/16.
 */
public class ProductImagesAdapter_2 extends RecyclerView.Adapter<ProductImagesAdapter_2.ViewHolder> {
    private Context context;
    private ArrayList<MediaMetadata> items;

    private int size;
    private boolean isInfiniteLoop;

    public ProductImagesAdapter_2(Context context, ArrayList<MediaMetadata> items) {
        this.context = context;
        this.items = items;
        this.size = ListUtils.getSize(items);
        isInfiniteLoop = false;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_product_image_2, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MediaMetadata item = items.get(position);
        Glide.with(context).load(item.getSecure_url()).thumbnail(0.1f).into(holder.img_category);
        //  ToDo: selection indicator for selected image, and selection on click
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.img_category)
        ImageView img_category;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}