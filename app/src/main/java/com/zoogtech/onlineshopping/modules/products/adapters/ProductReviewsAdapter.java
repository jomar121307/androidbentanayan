package com.zoogtech.onlineshopping.modules.products.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Review;
import com.zoogtech.onlineshopping.models.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rnecesito on 3/30/16.
 */
public class ProductReviewsAdapter extends RecyclerView.Adapter<ProductReviewsAdapter.ViewHolder> {
    Context context;
    ArrayList<Review> items;

    public ProductReviewsAdapter(Context context, ArrayList<Review> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_review, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Review item = items.get(position);
        User user = item.getUserId();
        if (user != null) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date post_date = null;
            try {
                post_date = format.parse(item.getCreatedAt());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date past = post_date;
            Date now = new Date();
            holder.label_timestamp.setText(DateUtils.getRelativeTimeSpanString(past.getTime(), now.getTime(), DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_NO_YEAR));
            if (user.getProfilePhoto().getSecure_url() != null) {
                Glide.with(context).load(user.getProfilePhoto().getSecure_url()).into(holder.img_user);
            } else {
                Glide.with(context).load(R.drawable.ph_user).into(holder.img_user);
            }
            holder.label_name.setText(user.getFirstName() + (user.getLastName() != null ? " " + user.getLastName() : ""));
            holder.label_message.setText(item.getContent());
            holder.rating_product.setRating(item.getRating());
            holder.label_rating.setText("Rating: " + item.getRating() + "/5.0");
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.img_user)
        ImageView img_user;
        @Bind(R.id.label_name)
        TextView label_name;
        @Bind(R.id.label_timestamp)
        TextView label_timestamp;
        @Bind(R.id.label_message)
        TextView label_message;
        @Bind(R.id.label_rating)
        TextView label_rating;
        @Bind(R.id.rating_product)
        RatingBar rating_product;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
