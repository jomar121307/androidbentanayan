package com.zoogtech.onlineshopping.modules.products;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.zoogtech.onlineshopping.R;

import butterknife.ButterKnife;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by rnecesito on 5/31/16.
 */
public class ProductImageFragment extends Fragment {

    ImageView mImageView;
    PhotoViewAttacher mAttacher;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fullimageview, container, false);

        // Any implementation of ImageView can be used!
        mImageView = ButterKnife.findById(v, R.id.iv_product);

        // Set the Drawable displayed
        String url = getArguments().getString("url");
        Glide.with(getActivity()).load(url).into(mImageView);

        // Attach a PhotoViewAttacher, which takes care of all of the zooming functionality.
        // (not needed unless you are going to change the drawable later)
        mAttacher = new PhotoViewAttacher(mImageView);

        return v;
    }
}
