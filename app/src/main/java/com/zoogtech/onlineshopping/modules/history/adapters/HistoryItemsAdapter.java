package com.zoogtech.onlineshopping.modules.history.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Order;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rnecesito on 3/30/16.
 */
public class HistoryItemsAdapter extends RecyclerView.Adapter<HistoryItemsAdapter.ViewHolder> {
    Context context;
    Order order;
    ArrayList<Order.Item> items;

    public HistoryItemsAdapter(Context context, Order order, ArrayList<Order.Item> items) {
        this.context = context;
        this.order = order;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_purchase_history, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Order.Item item = items.get(position);
        holder.label_item_name.setText(item.getProductId().getName());
        holder.label_item_seller.setText("By " + order.getShopId().getUserId().getFirstName() + (order.getShopId().getUserId().getLastName() != null ? " " + order.getShopId().getUserId().getLastName() : ""));
        holder.label_item_price.setText("₱" + String.format("%.02f", item.getProductId().getPrice()));
        Glide.with(context).load(item.getProductId().getMetadata().getPhotos().get(0).getSecure_url()).thumbnail(0.1f).into(holder.img_cart_item);
//        holder.label_order_date.setText(order.getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.img_cart_item)
        ImageView img_cart_item;
        @Bind(R.id.label_item_name)
        TextView label_item_name;
        @Bind(R.id.label_item_seller)
        TextView label_item_seller;
        @Bind(R.id.label_item_price)
        TextView label_item_price;
//        @Bind(R.id.label_order_date)
//        TextView label_order_date;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
