package com.zoogtech.onlineshopping.modules.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Spinner;
import android.widget.Toast;

import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.User;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 3/16/16.
 */
public class RegisterFragment extends Fragment {

    private static final String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";
    private static final String FNAME_PATTERN = "[A-Z][a-zA-Z]*";
    private static final String LNAME_PATTERN = "[a-zA-z]+([ '-][a-zA-Z]+)*";
    private Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
    private Pattern fnamePattern = Pattern.compile(FNAME_PATTERN);
    private Pattern lnamePattern = Pattern.compile(LNAME_PATTERN);
    private Matcher matcher;

    @Bind(R.id.usernameWrapper)
    TextInputLayout usernameWrapper;
    @Bind(R.id.emailWrapper)
    TextInputLayout emailWrapper;
    @Bind(R.id.passwordWrapper)
    TextInputLayout passwordWrapper;
    @Bind(R.id.confirmPasswordWrapper)
    TextInputLayout confirmPasswordWrapper;
    @Bind(R.id.firstNameWrapper)
    TextInputLayout firstNameWrapper;
    @Bind(R.id.lastNameWrapper)
    TextInputLayout lastNameWrapper;
    @Bind(R.id.gender)
    Spinner spinner_gender;

    private String username, email, password, confirmPassword, firstName, lastName, gender;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, v);

        usernameWrapper.setHint("Username");
        emailWrapper.setHint("Email");
        passwordWrapper.setHint("Password");
        confirmPasswordWrapper.setHint("Confrim Password");
        firstNameWrapper.setHint("First Name");
        lastNameWrapper.setHint("Last Name");

        return v;
    }

    @OnClick(R.id.login_btn)
    public void register(View v) {
        hideKeyboard();

        username = usernameWrapper.getEditText().getText().toString();
        email = emailWrapper.getEditText().getText().toString();
        password = passwordWrapper.getEditText().getText().toString();
        confirmPassword = confirmPasswordWrapper.getEditText().getText().toString();
        firstName = firstNameWrapper.getEditText().getText().toString();
        lastName = lastNameWrapper.getEditText().getText().toString();
        gender = spinner_gender.getSelectedItem().toString().toLowerCase();
        if (!validateUsername(username)) {
            usernameWrapper.setError("Enter a username");
        } else if (!validateEmail(email)) {
            emailWrapper.setError("Please enter an email address (e.g. alavares@test.com)");
        } else if (!validatePassword(password)) {
            passwordWrapper.setError("Password must be 6 characters or more");
        } else if (!matchPassword(password, confirmPassword)) {
            confirmPasswordWrapper.setError("Must match the given password");
        } else if (!validateFirstName(firstName)) {
            firstNameWrapper.setError("Please enter a proper first name");
        } else if (!validateLastName(lastName)) {
            lastNameWrapper.setError("Please enter a proper last name");
        } else {
            usernameWrapper.setErrorEnabled(false);
            emailWrapper.setErrorEnabled(false);
            passwordWrapper.setErrorEnabled(false);
            confirmPasswordWrapper.setErrorEnabled(false);
            firstNameWrapper.setErrorEnabled(false);
            lastNameWrapper.setErrorEnabled(false);

            doRegister();
        }
    }

    public boolean validateUsername(String username) {
        return username.length() > 0;
    }

    public boolean validateEmail(String email) {
        matcher = emailPattern.matcher(email);
        return matcher.matches();
    }

    public boolean validatePassword(String password) {
        return password.length() > 5;
    }

    public boolean matchPassword(String password, String confirmPassword) {
        return password.equals(confirmPassword);
    }

    public boolean validateFirstName(String fname) {
        matcher = lnamePattern.matcher(fname);
        return matcher.matches();
    }

    public boolean validateLastName(String lname) {
        matcher = lnamePattern.matcher(lname);
        return matcher.matches();
    }

    public void doRegister() {
        final ProgressDialog mDialog = new ProgressDialog(getContext(), ProgressDialog.STYLE_SPINNER);
        mDialog.setIndeterminate(true);
        mDialog.setMessage("Registering..");
        mDialog.show();
        ApiService apiService = new RestClient().getApiService();
        Call<User> call = apiService.createUser(
                email,
                username,
                password,
                confirmPassword,
                firstName,
                lastName,
                gender
        );
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                mDialog.dismiss();
                if (response.isSuccess()) {
                    Toast.makeText(getContext(), "Registration successful. Please login with your account.", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        Log.e("LOG", "Retrofit Response: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(getContext(), "Registration failed. Please try again.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                mDialog.dismiss();
                Toast.makeText(getContext(), "Registration failed. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
