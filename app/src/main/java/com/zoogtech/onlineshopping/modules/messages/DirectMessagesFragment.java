package com.zoogtech.onlineshopping.modules.messages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.PrivateMessage;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.modules.messages.adapters.DirectMessagesAdapter;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;
import com.zoogtech.onlineshopping.utils.Cache;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 3/30/16.
 */
public class DirectMessagesFragment extends Fragment {
    @Bind(R.id.rv_messages)
    RecyclerView rv_messages;
    @Bind(R.id.loader_layout)
    RelativeLayout loader_layout;
    @Bind(R.id.empty_cart_layout)
    CardView empty_cart_layout;
    @Bind(R.id.btn_action)
    Button btn_action;
    @Bind(R.id.main_message)
    TextView main_message;
    @Bind(R.id.sub_message)
    TextView sub_message;
    @Bind(R.id.ll_messagebox)
    LinearLayout ll_messagebox;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<PrivateMessage> orders;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_direct_messages, container, false);
        ButterKnife.bind(this, v);

        ll_messagebox.setVisibility(View.GONE);
        loader_layout.setVisibility(View.VISIBLE);
        ApiService apiService = new RestClient().getApiService();
        Session token = null;
        try {
            token = (Session) Cache.readObject(getContext(), "session");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        Call<ArrayList<PrivateMessage>> call = apiService.getMessages(
                "Bearer " + token.getAccessToken(),
                "updatedAt DESC"
        );
        call.enqueue(new Callback<ArrayList<PrivateMessage>>() {
            @Override
            public void onResponse(Call<ArrayList<PrivateMessage>> call, Response<ArrayList<PrivateMessage>> response) {
                loader_layout.setVisibility(View.GONE);
                if (response.isSuccess()) {
                    if (response.body().size() > 0) {
                        mLayoutManager = new LinearLayoutManager(getContext());
                        rv_messages.setLayoutManager(mLayoutManager);
                        mAdapter = new DirectMessagesAdapter(getContext(), response.body());
                        rv_messages.setAdapter(mAdapter);
                        mAdapter.notifyItemRangeChanged(0, response.body().size());
                        for (PrivateMessage p :
                                response.body()) {
                            ArrayList<PrivateMessage.User> users = p.getUsers();
//                            for (PrivateMessage.User u :
//                                    users) {
//                                if (u.getProfilePhoto().getSecure_url() != null) {
//                                    Log.i(u.getFirstName(), "HAS PHOTO");
//                                }
//                            }
                        }
                    } else {
                        empty_cart_layout.setVisibility(View.VISIBLE);
                        main_message.setText("No messages!");
                        sub_message.setText("You have no messages.");
                        btn_action.setVisibility(View.INVISIBLE);
                    }
                } else {
                    Log.i("HIST", response.message());
                    loader_layout.setVisibility(View.GONE);
                    empty_cart_layout.setVisibility(View.VISIBLE);
                    main_message.setText("An error occured!");
                    sub_message.setText("There was an error when accessing the server.");
                    btn_action.setText("Try Again");
                    btn_action.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            android.support.v4.app.FragmentTransaction ft;
                            DirectMessagesFragment f = new DirectMessagesFragment();
                            ft = getActivity().getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.frame, f);
                            ft.commit();
                        }
                    });
                }

            }

            @Override
            public void onFailure(Call<ArrayList<PrivateMessage>> call, Throwable t) {
                Log.i("MSG", t.getMessage());
                loader_layout.setVisibility(View.GONE);
                empty_cart_layout.setVisibility(View.VISIBLE);
                main_message.setText("An error occured!");
                sub_message.setText("There was an error when accessing the server.");
                btn_action.setText("Try Again");
                btn_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        android.support.v4.app.FragmentTransaction ft;
                        DirectMessagesFragment f = new DirectMessagesFragment();
                        ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame, f);
                        ft.commit();
                    }
                });
            }
        });

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Direct Feedback");
    }
}
