package com.zoogtech.onlineshopping.modules.products.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.zoogtech.onlineshopping.BaseActivity;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Product;
import com.zoogtech.onlineshopping.modules.products.ProductInfoFragment;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rnecesito on 3/17/16.
 */
public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {
    Context context;
    ArrayList<Product> products;

    public ProductListAdapter(Context context, ArrayList<Product> products) {
        this.context = context;
        this.products = products;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Product item = products.get(position);
        holder.label_description.setText(item.getDescription());
        holder.label_product.setText(item.getName());
        holder.label_price.setText(item.getCurrencyId().getSymbol() + String.format("%.02f", item.getPrice()));
        Glide.with(context).load(item.getMetadata().getPhotos().get(0).getSecure_url()).thumbnail(0.1f).into(holder.image_product);
        holder.product_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductInfoFragment fragment = new ProductInfoFragment();
                if (context == null)
                    return;
                if (context instanceof BaseActivity) {
                    Bundle b = new Bundle();
                    b.putSerializable("product", item);
                    fragment.setArguments(b);
                    BaseActivity mainActivity = (BaseActivity) context;
//                    mainActivity.switchContent(fragment, "productinfo");
                    mainActivity.switchContent(fragment);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.image_product)
        ImageView image_product;
        @Bind(R.id.label_product)
        TextView label_product;
        @Bind(R.id.label_description)
        TextView label_description;
        @Bind(R.id.label_price)
        TextView label_price;
        @Bind(R.id.product_container)
        MaterialRippleLayout product_container;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
