package com.zoogtech.onlineshopping.modules.products.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.zoogtech.onlineshopping.BaseActivity;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.MediaMetadata;
import com.zoogtech.onlineshopping.modules.products.ProductImageFragment;
import com.zoogtech.onlineshopping.utils.RecyclingPagerAdapter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.trinea.android.common.util.ListUtils;

/**
 * Created by rnecesito on 3/17/16.
 */
public class ProductImagesAdapter extends RecyclingPagerAdapter {
    private Context context;
    private ArrayList<MediaMetadata> items;

    private int size;
    private boolean isInfiniteLoop;

    public ProductImagesAdapter(Context context, ArrayList<MediaMetadata> items) {
        this.context = context;
        this.items = items;
        this.size = ListUtils.getSize(items);
        isInfiniteLoop = false;
    }

    @Override
    public int getCount() {
        return isInfiniteLoop ? Integer.MAX_VALUE : ListUtils.getSize(items);
    }

    private int getPosition(int position) {
        return isInfiniteLoop ? position % size : position;
    }


    @Override
    public View getView(int position, View view, ViewGroup container) {
        final MediaMetadata item = items.get(position);
        view = LayoutInflater.from(container.getContext()).inflate(R.layout.item_product_image, container, false);
        ViewHolder holder = new ViewHolder(view, container);
        Glide.with(context).load(item.getSecure_url()).thumbnail(0.1f).into(holder.img_category);
        holder.img_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductImageFragment fragment = new ProductImageFragment();
                if (context == null)
                    return;
                if (context instanceof BaseActivity) {
                    Bundle b = new Bundle();
                    b.putString("url", item.getSecure_url());
                    fragment.setArguments(b);
                    BaseActivity mainActivity = (BaseActivity) context;
//                    mainActivity.switchContent(fragment, "productimage");
                    mainActivity.switchContent(fragment);
                }
            }
        });
        return view;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.img_category)
        ImageView img_category;

        public ViewHolder(View itemView, ViewGroup parent) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * @return the isInfiniteLoop
     */
    public boolean isInfiniteLoop() {
        return isInfiniteLoop;
    }

    /**
     * @param isInfiniteLoop the isInfiniteLoop to set
     */
    public ProductImagesAdapter setInfiniteLoop(boolean isInfiniteLoop) {
        this.isInfiniteLoop = isInfiniteLoop;
        return this;
    }
}