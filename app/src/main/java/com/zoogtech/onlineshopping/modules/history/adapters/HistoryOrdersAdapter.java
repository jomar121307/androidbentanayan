package com.zoogtech.onlineshopping.modules.history.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.zoogtech.onlineshopping.BaseActivity;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Order;
import com.zoogtech.onlineshopping.modules.history.PurchaseHistoryItemsFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rnecesito on 3/29/16.
 */
public class HistoryOrdersAdapter extends RecyclerView.Adapter<HistoryOrdersAdapter.ViewHolder> {
    Context context;
    ArrayList<Order> orders;

    public HistoryOrdersAdapter(Context context, ArrayList<Order> orders) {
        this.context = context;
        this.orders = orders;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Order order = orders.get(position);
        holder.label_order_number.setText("Order #" + order.getId().substring(0, 9).toUpperCase());
        holder.label_order_status.setText("STATUS: " + order.getStatus().toUpperCase());
        holder.label_seller.setText("Seller: " + order.getShopId().getUserId().getFirstName() + (order.getShopId().getUserId().getLastName() != null ? " " + order.getShopId().getUserId().getLastName() : ""));
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("MMM dd, yyyy");
        try {
            Date when = format.parse(order.getCreatedAt());
            holder.label_order_date.setText("Purchase Date: " + format2.format(when));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.order_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PurchaseHistoryItemsFragment fragment = new PurchaseHistoryItemsFragment();
                if (context == null)
                    return;
                if (context instanceof BaseActivity) {
                    Bundle b = new Bundle();
                    b.putSerializable("order", order);
                    fragment.setArguments(b);
                    BaseActivity mainActivity = (BaseActivity) context;
//                    mainActivity.switchContent(fragment, "purchasehistoryitems");
                    mainActivity.switchContent(fragment);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.label_order_number)
        TextView label_order_number;
        @Bind(R.id.label_order_status)
        TextView label_order_status;
        @Bind(R.id.label_seller)
        TextView label_seller;
        @Bind(R.id.label_order_date)
        TextView label_order_date;
        @Bind(R.id.order_container)
        MaterialRippleLayout order_container;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
