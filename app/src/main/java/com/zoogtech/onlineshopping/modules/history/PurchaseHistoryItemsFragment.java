package com.zoogtech.onlineshopping.modules.history;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Address;
import com.zoogtech.onlineshopping.models.Order;
import com.zoogtech.onlineshopping.modules.history.adapters.HistoryItemsAdapter;
import com.zoogtech.onlineshopping.modules.home.HomeFragment;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rnecesito on 3/30/16.
 */
public class PurchaseHistoryItemsFragment extends Fragment {
    private static String TAG = "HISTORY";
    @Bind(R.id.rv_history)
    RecyclerView rv_cart;
    @Bind(R.id.empty_cart_layout)
    CardView empty_cart_layout;
    @Bind(R.id.btn_action)
    Button cart_to_home;
    @Bind(R.id.label_subtotal_price)
    TextView label_subtotal_price;
    @Bind(R.id.label_shipping_price)
    TextView label_shipping_price;
    @Bind(R.id.label_handling_price)
    TextView label_handling_price;
    @Bind(R.id.label_total_price)
    TextView label_total_price;
    @Bind(R.id.label_billing_address)
    TextView label_billing_address;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private Order order;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_purchase_history, container, false);
        ButterKnife.bind(this, v);

        order = (Order) getArguments().getSerializable("order");
        ArrayList items = order.getItems();
        if (items != null) {
            if (order.getItems().size() > 0) {
                mLayoutManager = new LinearLayoutManager(getContext());
                rv_cart.setLayoutManager(mLayoutManager);
                mAdapter = new HistoryItemsAdapter(getContext(), order, items);
                rv_cart.setAdapter(mAdapter);
                mAdapter.notifyItemRangeChanged(0, items.size());

                float subtotal = 0, shipping = 0, handling = 0, total = 0;
                for (Order.Item c : order.getItems()) {
                    subtotal += c.getProductId().getPrice() * c.getQuantity();
                    shipping += Float.parseFloat(c.getProductId().getMetadata().getShippingData()[0].getSingleCost());
                }
                handling = subtotal + shipping;
                handling = (handling * 1) / 100;
                handling += 0.3;
                total = subtotal + shipping + handling;
                label_subtotal_price.setText("₱" + String.format("%.02f", subtotal));
                label_shipping_price.setText("₱" + String.format("%.02f", shipping));
                label_handling_price.setText("₱" + String.format("%.02f", handling));
                label_total_price.setText("₱" + String.format("%.02f", total));
                Address address = order.getShippingId().getAddress();
                label_billing_address.setText(address.getFullName() + " " + address.getStreetAddress() + " " + address.getCity() + " " + address.getProvince() + " " + address.getCountry() + " " + address.getZipcode());
            } else {
                empty_cart_layout.setVisibility(View.VISIBLE);
                cart_to_home.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        android.support.v4.app.FragmentTransaction ft;
                        HomeFragment hf = new HomeFragment();
                        ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame, hf);
                        ft.commit();
                    }
                });
            }
        }

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Purchased Items");
    }
}
