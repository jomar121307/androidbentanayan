package com.zoogtech.onlineshopping.modules.products;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.CartAddResponse;
import com.zoogtech.onlineshopping.models.Product;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.modules.login.LoginMainFragment;
import com.zoogtech.onlineshopping.modules.products.adapters.ProductImagesAdapter_2;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;
import com.zoogtech.onlineshopping.utils.Cache;

import java.io.IOException;

import biz.kasual.materialnumberpicker.MaterialNumberPicker;
import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 4/4/16.
 */
public class ProductDetailsFragment extends Fragment {
    @Bind(R.id.label_id)
    TextView label_id;
    @Bind(R.id.label_stock)
    TextView label_stock;
    @Bind(R.id.product_description_full)
    TextView product_description_full;
    @Bind(R.id.label_price)
    TextView label_price;
    @Bind(R.id.product_photos)
    RecyclerView product_photos;
    @Bind(R.id.btn_add_to_cart)
    Button btn_add_to_cart;

    private Product item;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_product_details, container, false);
        ButterKnife.bind(this, v);

        item = OnlineShopping.getSelectedProduct();

        Session session = null;
        try {
            session = (Session) Cache.readObject(getContext(), "session");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (session != null) {
            if (item.getUserId().getId().equals(session.getUserId().getId())) {
                btn_add_to_cart.setEnabled(false);
                btn_add_to_cart.setVisibility(View.INVISIBLE);
            } else {
                btn_add_to_cart.setEnabled(true);
                btn_add_to_cart.setVisibility(View.VISIBLE);
            }
        } else {
            btn_add_to_cart.setText("Login to buy");
//            btn_add_to_cart.setEnabled(false);
            btn_add_to_cart.setVisibility(View.VISIBLE);
        }

        label_id.setText("Item #" + item.getId().substring(0, 9).toUpperCase());
        label_stock.setText("Available Stocks: " + item.getQuantity());
        product_description_full.setText(item.getDescription());
        label_price.setText(item.getCurrencyId().getSymbol() + item.getPrice());
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        product_photos.setLayoutManager(mLayoutManager);
        mAdapter = new ProductImagesAdapter_2(getContext(), item.getMetadata().getPhotos());
        product_photos.setAdapter(mAdapter);

        btn_add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OnlineShopping.getSession() != null) {
                    final MaterialNumberPicker numberPicker = new MaterialNumberPicker.Builder(getContext())
                            .minValue(1)
                            .maxValue(item.getQuantity())
                            .defaultValue(1)
                            .backgroundColor(Color.WHITE)
                            .separatorColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                            .textColor(Color.BLACK)
                            .textSize(20)
                            .enableFocusability(false)
                            .wrapSelectorWheel(true)
                            .build();
                    new AlertDialog.Builder(getContext())
                            .setTitle("Quantity to Add in Cart")
                            .setView(numberPicker)
                            .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    final ProgressDialog mDialog = new ProgressDialog(getContext(), ProgressDialog.STYLE_SPINNER);
                                    mDialog.setIndeterminate(true);
                                    mDialog.setMessage("Adding item/s to cart...");
                                    mDialog.show();
                                    Session token = null;
                                    try {
                                        token = (Session) Cache.readObject(getContext(), "session");
                                    } catch (IOException | ClassNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                    Log.i("PRODUCT ID", item.getId());
                                    ApiService apiService = new RestClient().getApiService();
                                    Call<CartAddResponse> call = apiService.addToCart(
                                            "Bearer " + token.getAccessToken(),
                                            item.getId(),
                                            numberPicker.getValue()
                                    );
                                    call.enqueue(new Callback<CartAddResponse>() {
                                        @Override
                                        public void onResponse(Call<CartAddResponse> call, Response<CartAddResponse> response) {
                                            mDialog.dismiss();
                                            if (response.isSuccess()) {
                                                Toast.makeText(getContext(), "Item added to cart!", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<CartAddResponse> call, Throwable t) {
                                            mDialog.dismiss();
                                            Toast.makeText(getContext(), "Error adding item to cart!", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            })
                            .show();
                } else {
                    android.support.v4.app.FragmentTransaction ft;
                    LoginMainFragment af = new LoginMainFragment();
                    ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.addToBackStack(null);
                    ft.remove(getParentFragment());
                    ft.replace(R.id.frame, af);
                    ft.commit();
                }
            }
        });

        return v;
    }
}
