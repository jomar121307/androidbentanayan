package com.zoogtech.onlineshopping.modules.payment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.AddAddress;
import com.zoogtech.onlineshopping.models.Address;
import com.zoogtech.onlineshopping.models.Shipping_2;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 3/23/16.
 */
public class NewAddressFragment extends Fragment {

    @Bind(R.id.fullnameWrapper)
    TextInputLayout fullnameWrapper;
    @Bind(R.id.addressWrapper)
    TextInputLayout addressWrapper;
    @Bind(R.id.cityWrapper)
    TextInputLayout cityWrapper;
    @Bind(R.id.provinceWrapper)
    TextInputLayout provinceWrapper;
    @Bind(R.id.zipcodeWrapper)
    TextInputLayout zipcodeWrapper;
    @Bind(R.id.fullname)
    EditText fullname;
    @Bind(R.id.address)
    EditText address;
    @Bind(R.id.city)
    EditText city;
    @Bind(R.id.province)
    EditText province;
    @Bind(R.id.zipcode)
    EditText zipcode;
    @Bind(R.id.country)
    Spinner country;


    String address_fullname;
    String address_address;
    String address_city;
    String address_province;
    String address_zipcode;
    String address_country;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_new_address, container, false);
        ButterKnife.bind(this, v);

        setHasOptionsMenu(true);
        String[] locales = Locale.getISOCountries();
        ArrayList<String> countries = new ArrayList<>();

        for (String countryCode : locales) {
            Locale obj = new Locale("", countryCode);
            countries.add(obj.getDisplayCountry());

        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, countries); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        country.setAdapter(spinnerArrayAdapter);
        address_country = countries.get(0);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Add Address");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_address_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                onBackPressed();
                return true;

            case R.id.menu_save_profile:
                address_fullname = fullnameWrapper.getEditText().getText().toString();
                address_address = addressWrapper.getEditText().getText().toString();
                address_city = cityWrapper.getEditText().getText().toString();
                address_province = provinceWrapper.getEditText().getText().toString();
                address_zipcode = zipcodeWrapper.getEditText().getText().toString();
                address_country = country.getSelectedItem().toString();
                if (address_fullname.trim().equals("")) {
                    fullnameWrapper.setError("Enter your full name");
                } else if (address_address.trim().equals("")) {
                    addressWrapper.setError("Enter your mailing address");
                } else if (address_city.trim().equals("")) {
                    cityWrapper.setError("Enter your current city");
                } else if (address_province.trim().equals("")) {
                    provinceWrapper.setError("Enter your current province!");
                } else if (address_zipcode.trim().equals("")) {
                    zipcodeWrapper.setError("Enter your current province's zipcode!");
                } else {
                    fullnameWrapper.setErrorEnabled(false);
                    addressWrapper.setErrorEnabled(false);
                    cityWrapper.setErrorEnabled(false);
                    provinceWrapper.setErrorEnabled(false);
                    zipcodeWrapper.setErrorEnabled(false);

                    final ProgressDialog mDialog = new ProgressDialog(getContext(), ProgressDialog.STYLE_SPINNER);
                    mDialog.setIndeterminate(true);
                    mDialog.setMessage("Adding address...");
                    mDialog.setCancelable(false);
                    mDialog.setCanceledOnTouchOutside(false);
                    mDialog.show();

                    HashMap<String, Address> main_map = new HashMap<>();
                    Map<String, String> map = new HashMap<>();
                    map.put("fullName", address_fullname);
                    map.put("streetAddress", address_address);
                    map.put("city", address_city);
                    map.put("province", address_province);
                    map.put("zipcode", address_zipcode);
                    map.put("country", address_country);
                    main_map.put("address", new Address(address_country, address_fullname, address_address, address_city, address_province, address_zipcode));
                    Log.i("MAPPP", main_map.toString());

                    JSONObject o = new JSONObject();
                    JSONObject o2 = new JSONObject();
                    try {
                        o2.put("fullName", address_fullname);
                        o2.put("streetAddress", address_address);
                        o2.put("city", address_city);
                        o2.put("province", address_province);
                        o2.put("zipcode", address_zipcode);
                        o2.put("country", address_country);
                        o.put("address", o2);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    ApiService service = new RestClient().getApiService();
                    Call<Shipping_2> call = service.addAddress(
                            "Bearer " + OnlineShopping.getSession().getAccessToken(),
                            new AddAddress(new Address(address_country, address_fullname, address_address, address_city, address_province, address_zipcode))
                    );
                    call.enqueue(new Callback<Shipping_2>() {
                        @Override
                        public void onResponse(Call<Shipping_2> call, Response<Shipping_2> response) {
                            mDialog.dismiss();
                            if (response.isSuccess()) {
                                android.support.v4.app.FragmentTransaction ft;
                                AddressFragment af = new AddressFragment();
                                ft = getActivity().getSupportFragmentManager().beginTransaction();
                                ft.replace(R.id.frame, af);
                                ft.commit();
                            } else {
                                try {
                                    Log.e("LOG", "Retrofit Response: " + response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                Toast.makeText(getContext(), "An error ocurred while adding a shipping address.", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Shipping_2> call, Throwable t) {
                            Log.i("ERRRRR", t.getMessage());
                            Toast.makeText(getContext(), "An error ocurred while adding a shipping address.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
