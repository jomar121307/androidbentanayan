package com.zoogtech.onlineshopping.modules.shop;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Product;
import com.zoogtech.onlineshopping.models.ProductsResponse;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.modules.products.adapters.ProductListAdapter;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;
import com.zoogtech.onlineshopping.utils.Cache;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 4/1/16.
 */
public class ShopFragment extends Fragment {
    @Bind(R.id.rv_products)
    RecyclerView rv_products;
    @Bind(R.id.loader_layout)
    RelativeLayout loader_layout;
    @Bind(R.id.empty_cart_layout)
    CardView empty_cart_layout;
    @Bind(R.id.btn_action)
    Button btn_action;
    @Bind(R.id.main_message)
    TextView main_message;
    @Bind(R.id.sub_message)
    TextView sub_message;

    private RecyclerView.LayoutManager mLayoutManager;
    private ProductListAdapter mAdapter;
    private ArrayList<Product> products;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_shop_productlist, container, false);
        ButterKnife.bind(this, v);

        loader_layout.setVisibility(View.VISIBLE);
        ApiService apiService = new RestClient().getApiService();
        Session session = null;
        try {
            session = (Session) Cache.readObject(getContext(), "session");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        Call<ProductsResponse> call = apiService.getUserProducts(
//                "Bearer " + session.getAccessToken(),
                ("{\"userId\" : \"" + OnlineShopping.getShopUser().getId() + "\"}")
        );
        call.enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                loader_layout.setVisibility(View.GONE);
                if (response.isSuccess()) {
                    if (response.body().getCount() > 0) {
                        mLayoutManager = new GridLayoutManager(getContext(), 2);
                        rv_products.setLayoutManager(mLayoutManager);
                        products = response.body().getProducts();
                        mAdapter = new ProductListAdapter(getContext(), products);
                        rv_products.setAdapter(mAdapter);
                        mAdapter.notifyItemRangeChanged(0, products.size());
                    } else {
                        empty_cart_layout.setVisibility(View.VISIBLE);
                        main_message.setText("This shop is empty.");
                        sub_message.setText("The owner does not have any items for you to purchase.");
                        btn_action.setVisibility(View.GONE);
                    }
                } else {
                    loader_layout.setVisibility(View.GONE);
                    empty_cart_layout.setVisibility(View.VISIBLE);
                    main_message.setText("An error occured!");
                    sub_message.setText("There was an error when accessing the server.");
                    btn_action.setText("Try Again");
                    btn_action.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            android.support.v4.app.FragmentTransaction ft;
                            ShopMainFragment f = new ShopMainFragment();
                            ft = getActivity().getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.frame, f);
                            ft.commit();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
                loader_layout.setVisibility(View.GONE);
                empty_cart_layout.setVisibility(View.VISIBLE);
                main_message.setText("An error occured!");
                sub_message.setText("There was an error when accessing the server.");
                btn_action.setText("Try Again");
                btn_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        android.support.v4.app.FragmentTransaction ft;
                        ShopMainFragment f = new ShopMainFragment();
                        ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame, f);
                        ft.commit();
                    }
                });
            }
        });

        return v;
    }

    String getShopId() {
        return getArguments().getString("shopId");
    }
}
