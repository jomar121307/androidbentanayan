package com.zoogtech.onlineshopping.modules.cart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.CartItem;
import com.zoogtech.onlineshopping.modules.cart.adapters.CartAdapter;
import com.zoogtech.onlineshopping.modules.home.HomeFragment;
import com.zoogtech.onlineshopping.modules.payment.AddressFragment;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rnecesito on 3/18/16.
 */
public class CartFragment extends Fragment {
    private static String TAG = "CART";
    @Bind(R.id.rv_cart)
    RecyclerView rv_cart;
    @Bind(R.id.empty_cart_layout)
    CardView empty_cart_layout;
    @Bind(R.id.btn_action)
    Button cart_to_home;
    @Bind(R.id.label_subtotal_price)
    TextView label_subtotal_price;
    @Bind(R.id.label_shipping_price)
    TextView label_shipping_price;
    @Bind(R.id.label_handling_price)
    TextView label_handling_price;
    @Bind(R.id.label_total_price)
    TextView label_total_price;
    @Bind(R.id.btn_checkout)
    Button btn_checkout;
    @Bind(R.id.btn_empty_cart)
    Button btn_empty_cart;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<CartItem> items;
    private static CartFragment cartFragment;

    public static CartFragment getInstance() {
        return cartFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cart, container, false);
        ButterKnife.bind(this, v);
        cartFragment = this;

        items = (ArrayList<CartItem>) getArguments().getSerializable("shopProducts");
        if (items != null) {
            if (items.size() > 0) {
                mLayoutManager = new LinearLayoutManager(getContext());
                rv_cart.setLayoutManager(mLayoutManager);
                mAdapter = new CartAdapter(getActivity(), items);
                rv_cart.setAdapter(mAdapter);
                mAdapter.notifyItemRangeChanged(0, items.size());
                float subtotal = 0, shipping = 0, handling = 0, total = 0;
                for (CartItem c : items) {
                    subtotal += c.getProductId().getPrice() * c.getQuantity();
                    shipping += Float.parseFloat(c.getProductId().getMetadata().getShippingData()[0].getSingleCost());
                }
                handling = subtotal + shipping;
                handling = (handling * 1) / 100;
                handling += 0.3;
                total = subtotal + shipping + handling;
                label_subtotal_price.setText("₱" + String.format("%.02f", subtotal));
                label_shipping_price.setText("₱" + String.format("%.02f", shipping));
                label_handling_price.setText("₱" + String.format("%.02f", handling));
                label_total_price.setText("₱" + String.format("%.02f", total));
                btn_checkout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        OnlineShopping.setCheckoutShopUser(items.get(0).getProductId().getUserId());
                        android.support.v4.app.FragmentTransaction ft;
                        AddressFragment af = new AddressFragment();
                        ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame, af);
                        ft.commit();
                    }
                });
            } else {
                empty_cart_layout.setVisibility(View.VISIBLE);
                cart_to_home.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        android.support.v4.app.FragmentTransaction ft;
                        HomeFragment hf = new HomeFragment();
                        ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame, hf);
                        ft.commit();
                    }
                });
            }
        }

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Cart");
    }

    public void updatePrices(ArrayList<CartItem> items) {
        float subtotal = 0, shipping = 0, handling = 0, total = 0;
        for (CartItem c : items) {
            subtotal += c.getProductId().getPrice() * c.getQuantity();
            shipping += Float.parseFloat(c.getProductId().getMetadata().getShippingData()[0].getSingleCost());
        }
        handling = subtotal + shipping;
        handling = (handling * 1) / 100;
        handling += 0.3;
        total = subtotal + shipping + handling;
        label_subtotal_price.setText("₱" + String.format("%.02f", subtotal));
        label_shipping_price.setText("₱" + String.format("%.02f", shipping));
        label_handling_price.setText("₱" + String.format("%.02f", handling));
        label_total_price.setText("₱" + String.format("%.02f", total));
    }
}
