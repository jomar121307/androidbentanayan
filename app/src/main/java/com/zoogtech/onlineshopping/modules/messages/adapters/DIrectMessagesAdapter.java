package com.zoogtech.onlineshopping.modules.messages.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.zoogtech.onlineshopping.BaseActivity;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.PrivateMessage;
import com.zoogtech.onlineshopping.modules.messages.MessagesFragment;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by rnecesito on 3/30/16.
 */
public class DirectMessagesAdapter extends RecyclerView.Adapter<DirectMessagesAdapter.ViewHolder> {
    Context context;
    ArrayList<PrivateMessage> items;

    public DirectMessagesAdapter(Context context, ArrayList<PrivateMessage> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_message_main, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PrivateMessage item = items.get(position);
        PrivateMessage.User user = null;
        ArrayList<PrivateMessage.User> users = item.getUsers();
        for (PrivateMessage.User u : users) {
            if (!u.is__self()) {
                user = u;
            }
        }
        holder.label_name.setText(user.getFirstName() + (user.getLastName() != null ? " " + user.getLastName() : ""));
//        Glide.with(context).load(user.getProfilePhoto().getSecure_url()).thumbnail(0.1f).into(holder.img_user);

        if (user.getProfilePhoto().getSecure_url() != null) {
            Glide.with(context).load(user.getProfilePhoto().getSecure_url()).into(holder.img_user);
        } else {
            Glide.with(context).load(R.drawable.ph_user).into(holder.img_user);
        }

        if (item.getUnreadCount() > 0) {
            holder.img_message.setImageResource(R.drawable.ic_message_active);
        } else {
            holder.img_message.setImageResource(R.drawable.ic_message);
        }
        holder.container_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessagesFragment fragment = new MessagesFragment();
                if (context == null)
                    return;
                if (context instanceof BaseActivity) {
                    Bundle b = new Bundle();
                    b.putSerializable("pm", item);
                    fragment.setArguments(b);
                    BaseActivity mainActivity = (BaseActivity) context;
//                    mainActivity.switchContent(fragment, "messages");
                    mainActivity.switchContent(fragment);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.img_user)
        CircleImageView img_user;
        @Bind(R.id.label_name)
        TextView label_name;
        @Bind(R.id.img_message)
        ImageView img_message;
        @Bind(R.id.container_message)
        MaterialRippleLayout container_message;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
