package com.zoogtech.onlineshopping.modules.payment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zoogtech.onlineshopping.BaseActivity;
import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Address;
import com.zoogtech.onlineshopping.models.CheckoutInfoResponse;
import com.zoogtech.onlineshopping.models.PayResponse;
import com.zoogtech.onlineshopping.models.PayUPSResponse;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.models.Shipping_2;
import com.zoogtech.onlineshopping.modules.home.HomeFragment;
import com.zoogtech.onlineshopping.modules.payment.adapters.AddressAdapter;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;
import com.zoogtech.onlineshopping.utils.Cache;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 3/23/16.
 */
public class AddressFragment extends Fragment {
    @Bind(R.id.rv_addresses)
    RecyclerView rv_addresses;
    @Bind(R.id.loader_layout)
    RelativeLayout loader_layout;
    @Bind(R.id.empty_cart_layout)
    CardView empty_cart_layout;
    @Bind(R.id.btn_action)
    Button btn_action;
    @Bind(R.id.main_message)
    TextView main_message;
    @Bind(R.id.sub_message)
    TextView sub_message;
    @Bind(R.id.btn_new_address)
    Button btn_new_address;
    @Bind(R.id.btn_choose_address)
    Button btn_choose_address;

    private RecyclerView.LayoutManager mLayoutManager;
    private AddressAdapter mAdapter;
    private ArrayList<Address> addresses;
    private ProgressDialog mDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_addresses, container, false);
        ButterKnife.bind(this, v);

        loader_layout.setVisibility(View.VISIBLE);
        ApiService apiService = new RestClient().getApiService();
        Session token = null;
        try {
            token = (Session) Cache.readObject(getContext(), "session");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        Call<ArrayList<Shipping_2>> call = apiService.getAddresses(
                "Bearer " + token.getAccessToken(),
                "20"
        );
        call.enqueue(new Callback<ArrayList<Shipping_2>>() {
            @Override
            public void onResponse(Call<ArrayList<Shipping_2>> call, Response<ArrayList<Shipping_2>> response) {
                loader_layout.setVisibility(View.GONE);
                if (response.isSuccess()) {
                    mLayoutManager = new LinearLayoutManager(getContext());
                    rv_addresses.setLayoutManager(mLayoutManager);
                    mAdapter = new AddressAdapter(getContext(), response.body());
                    rv_addresses.setAdapter(mAdapter);
                    mAdapter.notifyItemRangeChanged(0, response.body().size());
                } else {
                    empty_cart_layout.setVisibility(View.VISIBLE);
                    btn_action.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            android.support.v4.app.FragmentTransaction ft;
                            AddressFragment hf = new AddressFragment();
                            ft = getActivity().getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.frame, hf);
                            ft.commit();
                        }
                    });
                }

                btn_choose_address.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mAdapter.getAddressId() == "") {
                            Toast.makeText(getContext(), "Please select a shipping address", Toast.LENGTH_SHORT).show();
                        } else {
                            Boolean hasPaypal = false;
                            Boolean hasUPS = false;
                            for (String s : OnlineShopping.getCheckoutShopUser().getPaymentPlatformsAvailable()) {
                                if (s.equals("ecash")) {
                                    hasUPS = true;
                                }
                                if (s.equals("paypal")) {
                                    hasPaypal = true;
                                }
                            }
                            List<String> actions = new ArrayList<String>();
                            actions.add("Pay with E-Cash");
                            actions.add("Pay with PayPal");
                            final CharSequence[] actions1 = actions.toArray(new String[actions.size()]);
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                            dialogBuilder.setTitle("Choose a Payment Method");
                            final Boolean finalHasUPS = hasUPS;
                            final Boolean finalHasPaypal = hasPaypal;
                            dialogBuilder.setItems(actions1, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case 0:
                                            if (finalHasUPS) {
                                                final AlertDialog.Builder inputAlert = new AlertDialog.Builder(getContext());
                                                inputAlert.setTitle("E-Cash: Input Credentials");
                                                final EditText username = new EditText(getContext());
                                                username.setHint("Username");
                                                username.setInputType(InputType.TYPE_CLASS_TEXT);
                                                final EditText password = new EditText(getContext());
                                                password.setHint("Password");
                                                password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                                                LinearLayout linearLayout = new LinearLayout(getContext());
                                                linearLayout.setOrientation(LinearLayout.VERTICAL);
                                                linearLayout.addView(username);
                                                linearLayout.addView(password);
                                                inputAlert.setView(linearLayout, 50, 0, 50, 0);
                                                inputAlert.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        final String un = username.getText().toString();
                                                        final String pw = password.getText().toString();
                                                        if (un.trim().isEmpty() && pw.trim().isEmpty()) {
                                                            Toast.makeText(getContext(), "Please enter your credentials..", Toast.LENGTH_SHORT).show();
                                                        } else if (un.trim().isEmpty()) {
                                                            Toast.makeText(getContext(), "Please enter your username.", Toast.LENGTH_SHORT).show();
                                                        } else if (pw.trim().isEmpty()) {
                                                            Toast.makeText(getContext(), "Please enter your password.", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            mDialog = new ProgressDialog(getContext(), ProgressDialog.STYLE_SPINNER);
                                                            mDialog.setIndeterminate(true);
                                                            mDialog.setMessage("Selecting address..");
                                                            mDialog.setCancelable(false);
                                                            mDialog.show();
                                                            final Session token = OnlineShopping.getSession();
                                                            final ApiService apiService = new RestClient().getApiService();
                                                            Call<CheckoutInfoResponse> call = apiService.setCheckoutInfo(
                                                                    "Bearer " + token.getAccessToken(),
                                                                    OnlineShopping.getSelectedShop().getId(),
                                                                    mAdapter.getAddressId(),
                                                                    "ecash"
                                                            );
                                                            call.enqueue(new Callback<CheckoutInfoResponse>() {
                                                                @Override
                                                                public void onResponse(Call<CheckoutInfoResponse> call, Response<CheckoutInfoResponse> response) {
                                                                    if (response.isSuccess()) {
                                                                        mDialog.dismiss();
                                                                        mDialog.setIndeterminate(true);
                                                                        mDialog.setMessage("Processing..");
                                                                        mDialog.show();
                                                                        Call<PayUPSResponse> call2 = apiService.payWithUPS(
                                                                                "Bearer " + token.getAccessToken(),
                                                                                un,
                                                                                pw
                                                                        );
                                                                        call2.enqueue(new Callback<PayUPSResponse>() {
                                                                            @Override
                                                                            public void onResponse(Call<PayUPSResponse> call, Response<PayUPSResponse> response2) {
                                                                                if (response2.isSuccess()) {
                                                                                    if (response2.body().getStatus().equals("success")) {
                                                                                        mDialog.dismiss();
                                                                                        HomeFragment hf = new HomeFragment();
                                                                                        android.support.v4.app.FragmentTransaction ft;
                                                                                        getActivity().getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                                                                        ft = getActivity().getSupportFragmentManager().beginTransaction();
                                                                                        ft.remove(AddressFragment.this);
                                                                                        ft.replace(R.id.frame, hf);
                                                                                        ft.commit();
                                                                                        Toast.makeText(getContext(), "Payment with Ecash successful.", Toast.LENGTH_SHORT).show();
                                                                                    } else {
                                                                                        mDialog.dismiss();
                                                                                        String err = "An error ocurred. Please try again.";
                                                                                        try {
                                                                                            Log.e("LOG", "UPS Error Response: " + response2.errorBody().string());
                                                                                            err = response2.errorBody().string();
                                                                                            Toast.makeText(getContext(), err, Toast.LENGTH_SHORT).show();
                                                                                        } catch (IOException e) {
                                                                                            e.printStackTrace();
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    mDialog.dismiss();
                                                                                    String err = "An error ocurred. Please try again.";
                                                                                    try {
                                                                                        Log.e("LOG", "UPS Error Response 2: " + response2.errorBody().string());
                                                                                        err = response2.errorBody().string();
                                                                                        Toast.makeText(getContext(), err, Toast.LENGTH_SHORT).show();
                                                                                    } catch (IOException e) {
                                                                                        e.printStackTrace();
                                                                                    }
                                                                                }
                                                                            }

                                                                            @Override
                                                                            public void onFailure(Call<PayUPSResponse> call, Throwable t) {
                                                                                mDialog.dismiss();
                                                                                Log.e("UPS ERR", t.getMessage());
                                                                                Toast.makeText(getContext(), "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        });
                                                                    } else {
                                                                        mDialog.dismiss();
                                                                        Log.i("SetCheckoutInfo2", response.message());
                                                                        Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                                                                    }
                                                                }

                                                                @Override
                                                                public void onFailure(Call<CheckoutInfoResponse> call, Throwable t) {
                                                                    mDialog.dismiss();
                                                                    Log.i("SetCheckoutInfoError", t.getMessage());
                                                                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                                inputAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                                AlertDialog alertDialog = inputAlert.create();
                                                alertDialog.show();
                                            } else {
                                                Toast.makeText(getContext(), "Shop does not accept Ecash payments.", Toast.LENGTH_SHORT).show();
                                            }
                                            break;
                                        case 1:
                                            if (finalHasPaypal) {
                                                mDialog = new ProgressDialog(getContext(), ProgressDialog.STYLE_SPINNER);
                                                mDialog.setIndeterminate(true);
                                                mDialog.setMessage("Selecting address..");
                                                mDialog.setCancelable(false);
                                                mDialog.show();
                                                Session token = OnlineShopping.getSession();
                                                final ApiService apiService = new RestClient().getApiService();
                                                Call<CheckoutInfoResponse> call = apiService.setCheckoutInfo(
                                                        "Bearer " + token.getAccessToken(),
                                                        OnlineShopping.getSelectedShop().getId(),
                                                        mAdapter.getAddressId(),
                                                        "paypal"
                                                );
                                                final Session finalToken = token;
                                                call.enqueue(new Callback<CheckoutInfoResponse>() {
                                                    @Override
                                                    public void onResponse(Call<CheckoutInfoResponse> call, Response<CheckoutInfoResponse> response) {
                                                        if (response.isSuccess()) {
                                                            if (response.body().getStatus().equals("success")) {
                                                                Log.i("SUCCESS", "YEYYYY");
                                                                mDialog.dismiss();
                                                                mDialog.setIndeterminate(true);
                                                                mDialog.setMessage("Processing..");
                                                                mDialog.show();
                                                                Call<PayResponse> call2 = apiService.pay(
                                                                        "Bearer " + finalToken.getAccessToken()
                                                                );
                                                                call2.enqueue(new Callback<PayResponse>() {
                                                                    @Override
                                                                    public void onResponse(Call<PayResponse> call2, Response<PayResponse> response2) {
                                                                        if (response2.isSuccess()) {
                                                                            mDialog.dismiss();
                                                                            PayPalFragment pf = new PayPalFragment();
                                                                            Bundle b = new Bundle();
                                                                            b.putSerializable("url", response2.body().getPaymentApprovalUrl());
                                                                            pf.setArguments(b);
                                                                            FragmentManager fm = getActivity().getSupportFragmentManager();
                                                                            fm.beginTransaction().replace(R.id.frame, pf).commit();
                                                                        } else {
                                                                            mDialog.dismiss();
                                                                            try {
                                                                                Log.e("LOG", "PayPal Error Response: " + response2.errorBody().string());
                                                                            } catch (IOException e) {
                                                                                e.printStackTrace();
                                                                            }
                                                                            Toast.makeText(getContext(), "An error ocurred.", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<PayResponse> call2, Throwable t2) {
                                                                        mDialog.dismiss();
                                                                        Log.i("PayPal Error", t2.getMessage());
                                                                        Toast.makeText(getContext(), t2.getMessage(), Toast.LENGTH_SHORT).show();
                                                                    }
                                                                });
                                                            }
                                                        } else {
                                                            mDialog.dismiss();
                                                            Log.i("SetCheckoutInfo2", response.message());
                                                            Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<CheckoutInfoResponse> call, Throwable t) {
                                                        Log.i("SetCheckoutInfoError", t.getMessage());
                                                        Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                            } else {
                                                Toast.makeText(getContext(), "Shop does not accept payments through PayPal.", Toast.LENGTH_SHORT).show();
                                            }
                                            break;
                                    }
                                }
                            });
                            AlertDialog alertDialogObject = dialogBuilder.create();
                            alertDialogObject.show();
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<ArrayList<Shipping_2>> call, Throwable t) {
                Log.e("ADDRESS ERR", t.getMessage());
                loader_layout.setVisibility(View.GONE);
                empty_cart_layout.setVisibility(View.VISIBLE);
                main_message.setText("An error occured!");
                sub_message.setText("There was an error when accessing the server.");
                btn_action.setText("Try Again");
                btn_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        android.support.v4.app.FragmentTransaction ft;
                        AddressFragment af = new AddressFragment();
                        ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame, af);
                        ft.commit();
                    }
                });
            }
        });

        btn_new_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewAddressFragment fragment = new NewAddressFragment();
                if (getActivity() == null)
                    return;
                if (getActivity() instanceof BaseActivity) {
                    BaseActivity mainActivity = (BaseActivity) getActivity();
//                    mainActivity.switchContent(fragment, "newaddress");
                    mainActivity.switchContent(fragment);
                }
            }
        });

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Select Address");
    }
}
