package com.zoogtech.onlineshopping.modules.home.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Category;
import com.zoogtech.onlineshopping.utils.RecyclingPagerAdapter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.trinea.android.common.util.ListUtils;

/**
 * Created by rnecesito on 3/17/16.
 */
public class CategoryAdapter extends RecyclingPagerAdapter {
    private Context context;
    private ArrayList<Category> items;

    private int size;
    private boolean isInfiniteLoop;

    public CategoryAdapter(Context context, ArrayList<Category> items) {
        this.context = context;
        this.items = items;
        this.size = ListUtils.getSize(items);
        isInfiniteLoop = false;
    }

    @Override
    public int getCount() {
        return isInfiniteLoop ? Integer.MAX_VALUE : ListUtils.getSize(items);
    }

    private int getPosition(int position) {
        return isInfiniteLoop ? position % size : position;
    }


    @Override
    public View getView(int position, View view, ViewGroup container) {
        Category item = items.get(position);
        view = LayoutInflater.from(container.getContext()).inflate(R.layout.item_category, container, false);
        ViewHolder holder = new ViewHolder(view, container);
        holder.label_category.setText(item.getName());
        Glide.with(context).load(item.getThumbnail()).into(holder.img_category);
        Log.i("PHOTO", new Gson().toJson(item.getThumbnail()));
        return view;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.img_category)
        ImageView img_category;
        @Bind(R.id.label_category)
        TextView label_category;

        public ViewHolder(View itemView, ViewGroup parent) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * @return the isInfiniteLoop
     */
    public boolean isInfiniteLoop() {
        return isInfiniteLoop;
    }

    /**
     * @param isInfiniteLoop the isInfiniteLoop to set
     */
    public CategoryAdapter setInfiniteLoop(boolean isInfiniteLoop) {
        this.isInfiniteLoop = isInfiniteLoop;
        return this;
    }
}
