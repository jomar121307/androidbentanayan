package com.zoogtech.onlineshopping.modules.products;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.zoogtech.onlineshopping.BaseActivity;
import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Product;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.modules.products.adapters.ProductImagesAdapter;
import com.zoogtech.onlineshopping.modules.products.adapters.ProductPagerAdapter;
import com.zoogtech.onlineshopping.modules.shop.ShopMainFragment;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;
import com.zoogtech.onlineshopping.utils.Cache;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 3/17/16.
 */
public class ProductInfoFragment extends Fragment {
    @Bind(R.id.images_product)
    AutoScrollViewPager images_product;
    @Bind(R.id.rating_product)
    RatingBar rating_product;
    @Bind(R.id.label_product)
    TextView label_product;
    @Bind(R.id.label_product_owner)
    TextView label_product_owner;
    @Bind(R.id.info)
    ImageView info;
    @Bind(R.id.comments)
    ImageView comments;
    @Bind(R.id.more_options)
    ImageView more_options;
    @Bind(R.id.product_viewpager)
    ViewPager product_viewpager;


    private Product item;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this, v);

        item = (Product) getArguments().getSerializable("product");
        final ProgressDialog mDialog = new ProgressDialog(getContext(), ProgressDialog.STYLE_SPINNER);
        mDialog.setIndeterminate(true);
        mDialog.setMessage("Fetching data...");
        mDialog.show();
        ApiService apiService = new RestClient().getApiService();
        Call<Product> call = apiService.getSingleProduct(item.getId());
        call.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                mDialog.dismiss();
                if (response.isSuccess()) {
                    item = response.body();
                    OnlineShopping.setSelectedProduct(response.body());
                    if (item != null) {
                        images_product.setAdapter(new ProductImagesAdapter(getContext(), item.getMetadata().getPhotos()).setInfiniteLoop(false));
                        label_product.setText(item.getName());
                        Session session = null;
                        try {
                            session = (Session) Cache.readObject(getContext(), "session");
                        } catch (IOException | ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                        if (session != null) {
                            if (item.getUserId().getId().equals(session.getUserId().getId())) {
                                label_product_owner.setText("Owned by you");
                            } else {
                                label_product_owner.setText("By " + item.getUserId().getFirstName() + (item.getUserId().getLastName() != null ? " " + item.getUserId().getLastName() : ""));
                            }
                        } else {
                            label_product_owner.setText("By " + item.getUserId().getFirstName() + (item.getUserId().getLastName() != null ? " " + item.getUserId().getLastName() : ""));
                        }
                        more_options.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final CharSequence[] listItems = {
                                        "Visit Owner Shop"
                                };

                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                builder.setTitle("More Options");
                                builder.setItems(listItems, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int index) {
                                        if (listItems[0] == "Visit Owner Shop") {
                                            ShopMainFragment fragment = new ShopMainFragment();
                                            if (getContext() != null) {
                                                Bundle b = new Bundle();
                                                b.putSerializable("user", item.getUserId());
                                                b.putSerializable("shop", item.getShopId());
                                                fragment.setArguments(b);
                                                BaseActivity mainActivity = (BaseActivity) getContext();
//                                                mainActivity.switchContent(fragment, "shopmain");
                                                mainActivity.switchContent(fragment);
                                            }
                                        }
                                    }
                                });
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });
                    }
                    product_viewpager.setAdapter(new ProductPagerAdapter(getChildFragmentManager()));
                    info.setImageResource(R.drawable.ic_info_active);
                    info.setBackgroundColor(Color.parseColor("#F1F1F1"));
                    product_viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

                        @Override
                        public void onPageSelected(int position) {
                            if (position == 0) {
                                info.setImageResource(R.drawable.ic_info_active);
                                info.setBackgroundColor(Color.parseColor("#F1F1F1"));
                                comments.setImageResource(R.drawable.ic_comments);
                                comments.setBackgroundColor(Color.parseColor("#FFFFFF"));
                                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Product Details");
                            } else {
                                info.setImageResource(R.drawable.ic_info);
                                info.setBackgroundColor(Color.parseColor("#FFFFFF"));
                                comments.setImageResource(R.drawable.ic_comments_active);
                                comments.setBackgroundColor(Color.parseColor("#F1F1F1"));
                                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Product Reviews");
                            }
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {}
                    });
                    rating_product.setRating(item.getReviewScore());
                    Drawable progress = rating_product.getProgressDrawable();
//                    DrawableCompat.setTint(progress, Color.YELLOW);
                } else {
                    mDialog.dismiss();
                    getFragmentManager().popBackStack();
                    Toast.makeText(getContext(), "An error ocurred while fetching the product's information.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                mDialog.dismiss();
                getFragmentManager().popBackStack();
                Toast.makeText(getContext(), "An error ocurred while fetching the product's information.", Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (item != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(item.getName());
        }
    }

}
