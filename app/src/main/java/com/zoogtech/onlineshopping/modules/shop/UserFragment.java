package com.zoogtech.onlineshopping.modules.shop;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Message;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.models.User;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;
import com.zoogtech.onlineshopping.utils.Cache;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 4/1/16.
 */
public class UserFragment extends Fragment {
    @Bind(R.id.firstname)
    EditText firstname;
    @Bind(R.id.middlename)
    EditText middlename;
    @Bind(R.id.lastname)
    EditText lastname;
    @Bind(R.id.email)
    EditText email;
    @Bind(R.id.mobile)
    EditText mobile;
    @Bind(R.id.birthday)
    EditText birthday;
    @Bind(R.id.message_user)
    Button message_user;
    @Bind(R.id.label_shop)
    TextView label_shop;
    @Bind(R.id.img_user)
    CircleImageView img_user;

    private User user;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_shop, container, false);
        ButterKnife.bind(this, v);

        if (OnlineShopping.getShopUser() != null) {
            user = OnlineShopping.getShopUser();
            firstname.setEnabled(false);
            firstname.setHint(user.getFirstName());
            middlename.setEnabled(false);
            middlename.setHint(user.getMiddleName());
            lastname.setEnabled(false);
            lastname.setHint(user.getLastName() != null ? user.getLastName() : "");
            email.setEnabled(false);
            email.setHint(user.getEmail());
            mobile.setEnabled(false);
            birthday.setEnabled(false);
            birthday.setHint(user.getBirthday() != null ? user.getBirthday() : "");
            Glide.with(getContext()).load(user.getProfilePhoto().getSecure_url()).into(img_user);
            label_shop.setText(user.getFirstName() + "\'s Shop");
            if (user.getId().equals(OnlineShopping.getSession().getUserId().getId())) {
                message_user.setVisibility(View.GONE);
            }
        }

        message_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OnlineShopping.getSession() != null) {
                    OpenCategroyDialogBox();
                } else {
                    Toast.makeText(getContext(), "You need to be logged in to message this user.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return v;
    }

    private void OpenCategroyDialogBox() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View promptView = layoutInflater.inflate(R.layout.layout_message_owner, null);
        final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Send A Message");
        alert.setView(promptView);
        final EditText input = (EditText) promptView.findViewById(R.id.etCategory);
        input.requestFocus();
        input.setHint("Enter your message");
        input.setTextColor(Color.BLACK);

        alert.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }

                }

        );

        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Log.i("Message", "Cancelled");
                    }
                }

        );

        final AlertDialog alert1 = alert.create();
        alert1.show();

        alert1.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = input.getText().toString();
                if (!message.trim().isEmpty()) {
                    input.setEnabled(false);
                    ApiService apiService = new RestClient().getApiService();
                    Session session = null;
                    try {
                        session = (Session) Cache.readObject(getContext(), "session");
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    try {
                        Call<Message> call = apiService.startMessage(
                                "Bearer " + session.getAccessToken(),
                                user.getId(),
                                new JSONObject("{'message':'" + message + "'}")
                        );
                        call.enqueue(new Callback<Message>() {
                            @Override
                            public void onResponse(Call<Message> call, Response<Message> response) {
                                input.setEnabled(true);
                                if (response.isSuccess()) {
                                    alert1.dismiss();
                                    Toast.makeText(getContext(), "Message sent.", Toast.LENGTH_LONG).show();
                                } else {
                                    Log.i("MESSAGE", response.message());
                                    Toast.makeText(getContext(), "An error ocurred while posting your message.", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Message> call, Throwable t) {
                                input.setEnabled(true);
                                Log.i("MESSAGE", t.getLocalizedMessage());
                                Toast.makeText(getContext(), "An error ocurred while posting your message.", Toast.LENGTH_LONG).show();
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }
}
