package com.zoogtech.onlineshopping.modules.cart.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.zoogtech.onlineshopping.BaseActivity;
import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.CartAddResponse;
import com.zoogtech.onlineshopping.models.CartAddResponse_2;
import com.zoogtech.onlineshopping.models.CartItem;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.modules.cart.CartFragment;
import com.zoogtech.onlineshopping.modules.cart.CartShopsFragment;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;
import com.zoogtech.onlineshopping.utils.Cache;

import java.io.IOException;
import java.util.ArrayList;

import biz.kasual.materialnumberpicker.MaterialNumberPicker;
import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 3/18/16.
 */
public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    Context context;
    ArrayList<CartItem> products;

    public CartAdapter(Context context, ArrayList<CartItem> products) {
        this.context = context;
        this.products = products;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cart, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CartItem item = products.get(position);
        holder.label_cart_item.setText(item.getProductId().getName());
        holder.label_cart_item_seller.setText(item.getProductId().getUserId().getFirstName() + (item.getProductId().getUserId().getLastName() != null ? " " + item.getProductId().getUserId().getLastName() : ""));
//        holder.label_cart_item_price.setText(item.getProductId().getCurrencyId().getSymbol() + String.format("%.02f", item.getProductId().getPrice()));
        holder.label_cart_item_price.setText("₱" + String.format("%.02f", item.getProductId().getPrice()));
        Glide.with(context).load(item.getProductId().getMetadata().getPhotos().get(0).getSecure_url()).into(holder.img_cart_item);
                holder.label_qty.setText("Quantity: " + item.getQuantity() + "/" + item.getProductId().getQuantity());
        holder.label_item_total.setText("₱" + String.format("%.02f", (item.getProductId().getPrice() * item.getQuantity())));
        holder.ll_cart_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MaterialNumberPicker numberPicker = new MaterialNumberPicker.Builder(context)
                        .minValue(1)
                        .maxValue(100)
                        .defaultValue(1)
                        .backgroundColor(Color.WHITE)
                        .separatorColor(ContextCompat.getColor(context, R.color.colorPrimary))
                        .textColor(Color.BLACK)
                        .textSize(20)
                        .enableFocusability(false)
                        .wrapSelectorWheel(true)
                        .build();
                new AlertDialog.Builder(context)
                        .setTitle("Change cart item quantity")
                        .setView(numberPicker)
                        .setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final ProgressDialog mDialog = new ProgressDialog(context, ProgressDialog.STYLE_SPINNER);
                                mDialog.setIndeterminate(true);
                                mDialog.setMessage("Updating item quantity...");
                                mDialog.show();
                                Session token = OnlineShopping.getSession();
                                ApiService apiService = new RestClient().getApiService();
                                Call<CartAddResponse_2> call = apiService.updateCartItem(
                                        "Bearer " + token.getAccessToken(),
                                        item.getId(),
                                        numberPicker.getValue()
                                );
                                call.enqueue(new Callback<CartAddResponse_2>() {
                                    @Override
                                    public void onResponse(Call<CartAddResponse_2> call, Response<CartAddResponse_2> response) {
                                        mDialog.dismiss();
                                        if (response.isSuccess()) {
                                            item.setQuantity(numberPicker.getValue());
                                            notifyDataSetChanged();
                                            CartFragment.getInstance().updatePrices(products);
                                            Toast.makeText(context, "Item quantity updated!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            try {
                                                Log.e("LOG", "Retrofit Response: " + response.errorBody().string());
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CartAddResponse_2> call, Throwable t) {
                                        mDialog.dismiss();
                                        Log.e("ERRRORRR", t.getMessage());
                                        Toast.makeText(context, "An error occurred while updating the item quantity!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        })
                        .show();
            }
        });
        holder.remove_cart_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context, 0);
                builder.setTitle("Remove item from cart");
                builder.setMessage("Are you sure you want to remove this item from your cart? (You cannot undo this action)");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final ProgressDialog mDialog = new ProgressDialog(context, ProgressDialog.STYLE_SPINNER);
                        mDialog.setIndeterminate(true);
                        mDialog.setMessage("Removing from cart...");
                        mDialog.show();
                        Session session = null;
                        try {
                            session = (Session) Cache.readObject(context, "session");
                        } catch (IOException | ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                        ApiService apiService = new RestClient().getApiService();
                        Call<CartAddResponse> call = apiService.removeFromCart(
                                "Bearer " + session.getAccessToken(),
                                item.getId()
                        );
                        call.enqueue(new Callback<CartAddResponse>() {
                            @Override
                            public void onResponse(Call<CartAddResponse> call, Response<CartAddResponse> response) {
                                mDialog.dismiss();
                                if (response.isSuccess()) {
                                    int index = 0;
                                    for (CartItem i : products) {
                                        if (i.getId().equals(response.body().getId())) {
                                            products.remove(index);
                                            notifyItemRemoved(index);
                                            CartFragment.getInstance().updatePrices(products);
                                            if (products.size() == 0) {
                                                CartShopsFragment fragment = new CartShopsFragment();
                                                if (context == null) {
                                                    return;
                                                } else {
                                                    BaseActivity mainActivity = (BaseActivity) context;
                                                    android.support.v4.app.FragmentTransaction ft;
                                                    ft = mainActivity.getSupportFragmentManager().beginTransaction();
                                                    ft.replace(R.id.frame, fragment);
                                                    ft.commit();
                                                }
                                            }
                                        }
                                        index++;
                                    }
                                } else {
                                    Toast.makeText(context, "An error occurred while removing the item from your cart.", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<CartAddResponse> call, Throwable t) {
                                mDialog.dismiss();
                                Toast.makeText(context, "An error occurred while removing the item from your cart.", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.label_cart_item)
        TextView label_cart_item;
        @Bind(R.id.label_cart_item_seller)
        TextView label_cart_item_seller;
        @Bind(R.id.label_cart_item_price)
        TextView label_cart_item_price;
        @Bind(R.id.img_cart_item)
        ImageView img_cart_item;
        @Bind(R.id.label_qty)
        TextView label_qty;
        @Bind(R.id.label_item_total)
        TextView label_item_total;
        @Bind(R.id.remove_cart_item)
        ImageView remove_cart_item;
        @Bind(R.id.ll_cart_item)
        LinearLayout ll_cart_item;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
