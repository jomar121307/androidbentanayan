package com.zoogtech.onlineshopping.modules.messages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Message;
import com.zoogtech.onlineshopping.models.PrivateMessage;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.modules.messages.adapters.MessagesAdapter;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;
import com.zoogtech.onlineshopping.utils.Cache;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 3/30/16.
 */
public class MessagesFragment extends Fragment {
    @Bind(R.id.rv_messages)
    RecyclerView rv_messages;
    @Bind(R.id.ll_messagebox)
    LinearLayout ll_messagebox;
    @Bind(R.id.et_reply)
    EditText et_reply;
    @Bind(R.id.img_send)
    ImageView img_send;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private PrivateMessage privateMessage;
    private ArrayList<Message> messages;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_direct_messages, container, false);
        ButterKnife.bind(this, v);

        ll_messagebox.setVisibility(View.VISIBLE);
        privateMessage = (PrivateMessage) getArguments().getSerializable("pm");
        if (privateMessage != null) {
            if (privateMessage.getMessageCount() > 0) {
                messages = privateMessage.getMessages();
                mLayoutManager = new LinearLayoutManager(getContext());
                rv_messages.setLayoutManager(mLayoutManager);
                mAdapter = new MessagesAdapter(getContext(), messages, privateMessage.getUsers());
                rv_messages.setAdapter(mAdapter);
                mAdapter.notifyItemRangeChanged(0, privateMessage.getMessageCount());
                rv_messages.scrollToPosition(messages.size()-1);
                img_send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String message = et_reply.getText().toString();
                        if (!message.trim().isEmpty()) {
                            et_reply.setEnabled(false);
                            ApiService apiService = new RestClient().getApiService();
                            Session session = null;
                            try {
                                session = (Session) Cache.readObject(getContext(), "session");
                            } catch (IOException | ClassNotFoundException e) {
                                e.printStackTrace();
                            }
                            PrivateMessage.User user = null;
                            for (PrivateMessage.User u : privateMessage.getUsers()) {
                                if (!session.getUserId().getId().equals(u.getId())) {
                                    user = u;
                                }
                            }
                            try {
                                Call<Message> call = apiService.postMessage(
                                        "Bearer " + session.getAccessToken(),
                                        user.getId(),
                                        privateMessage.getId(),
                                        new JSONObject("{'message':'" + message + "'}")
                                );
                                call.enqueue(new Callback<Message>() {
                                    @Override
                                    public void onResponse(Call<Message> call, Response<Message> response) {
                                        et_reply.setEnabled(true);
                                        if (response.isSuccess()) {
                                            et_reply.setText("");
                                            messages.add(response.body());
                                            mAdapter.notifyItemInserted(messages.size() - 1);
                                        } else {
                                            Toast.makeText(getContext(), "An error ocurred while posting your message.", Toast.LENGTH_LONG).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Message> call, Throwable t) {
                                        et_reply.setEnabled(true);
                                        Toast.makeText(getContext(), "An error ocurred while posting your message.", Toast.LENGTH_LONG).show();
                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }

        return v;
    }
}
