package com.zoogtech.onlineshopping.modules.messages.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Message;
import com.zoogtech.onlineshopping.models.PrivateMessage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rnecesito on 3/30/16.
 */
public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {
    Context context;
    ArrayList<Message> items;
    ArrayList<PrivateMessage.User> users;

    public MessagesAdapter(Context context, ArrayList<Message> items, ArrayList<PrivateMessage.User> users) {
        this.context = context;
        this.items = items;
        this.users = users;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_message_sub, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Message item = items.get(position);
        PrivateMessage.User user = null;
        for (PrivateMessage.User u : users) {
            if (item.getFrom().equals(u.getId())) {
                user = u;
            }
        }
        if (user != null) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            try {
                Date past = format.parse(item.getCreatedAt());
                Date now = new Date();
                holder.label_timestamp.setText(DateUtils.getRelativeTimeSpanString(past.getTime(), now.getTime(), DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_NO_YEAR));
                if (user.getProfilePhoto().getSecure_url() != null) {
                    Glide.with(context).load(user.getProfilePhoto().getSecure_url()).into(holder.img_user);
                } else {
                    Glide.with(context).load(R.drawable.ph_user).into(holder.img_user);
                }
                holder.label_name.setText(user.getFirstName() + (user.getLastName() != null ? " " + user.getLastName() : ""));
                holder.label_message.setText(item.getPayload().getMessage());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.img_user)
        ImageView img_user;
        @Bind(R.id.label_name)
        TextView label_name;
        @Bind(R.id.label_timestamp)
        TextView label_timestamp;
        @Bind(R.id.label_message)
        TextView label_message;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
