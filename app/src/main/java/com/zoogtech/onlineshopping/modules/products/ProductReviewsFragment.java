package com.zoogtech.onlineshopping.modules.products;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Product;
import com.zoogtech.onlineshopping.models.Review;
import com.zoogtech.onlineshopping.models.ReviewResponse;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.modules.products.adapters.ProductReviewsAdapter;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;
import com.zoogtech.onlineshopping.utils.Cache;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 4/4/16.
 */
public class ProductReviewsFragment extends Fragment {
    @Bind(R.id.rv_reviews)
    RecyclerView rv_reviews;
    @Bind(R.id.et_review)
    EditText et_review;
    @Bind(R.id.img_send)
    ImageView img_send;
    @Bind(R.id.rating_product)
    RatingBar rating_product;

    private ArrayList<Review> reviews;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private Product product;
    private ProgressDialog mDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_product_reviews, container, false);
        ButterKnife.bind(this, v);

        product = OnlineShopping.getSelectedProduct();
        reviews = product.getReviews();

        if (reviews != null) {
            if (reviews.size() > 0) {
                mLayoutManager = new LinearLayoutManager(getContext());
                rv_reviews.setLayoutManager(mLayoutManager);
                mAdapter = new ProductReviewsAdapter(getContext(), reviews);
                rv_reviews.setAdapter(mAdapter);
                mAdapter.notifyItemRangeChanged(0, reviews.size());
            }
            if (OnlineShopping.getSession() == null) {
                et_review.setEnabled(false);
                et_review.setHint("You must be logged in to make a review");
                img_send.setEnabled(false);
            } else {
                et_review.setEnabled(true);
                et_review.setHint("Type your review here");
                img_send.setEnabled(true);
            }
            img_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String message = et_review.getText().toString();
                    if (!message.trim().isEmpty()) {
                        et_review.setEnabled(false);
                        img_send.setEnabled(false);
                        ApiService apiService = new RestClient().getApiService();
                        Session session = null;
                        try {
                            session = (Session) Cache.readObject(getContext(), "session");
                        } catch (IOException | ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                        String content = et_review.getText().toString();
                        float rating = rating_product.getRating();
                        Call<ReviewResponse> call = apiService.postReview(
                                "Bearer " + session.getAccessToken(),
                                product.getId(),
                                session.getUserId().getId(),
                                content,
                                "zcommerce",
                                rating
                        );
                        final Session finalSession = session;
                        call.enqueue(new Callback<ReviewResponse>() {
                            @Override
                            public void onResponse(Call<ReviewResponse> call, Response<ReviewResponse> response) {
                                et_review.setEnabled(true);
                                img_send.setEnabled(true);
                                if (response.isSuccess()) {
                                    et_review.setText("");
                                    ReviewResponse body = response.body();
                                    Review rev = new Review(
                                            body.getProductId(),
                                            finalSession.getUserId(),
                                            body.getContent(),
                                            body.isReply(),
                                            body.getRating(),
                                            body.getMetadata(),
                                            body.getCreatedAt(),
                                            body.getUpdatedAt(),
                                            body.getId()
                                    );
                                    reviews.add(rev);
                                    mAdapter.notifyItemInserted(reviews.size() - 1);
                                    Toast.makeText(getContext(), "Review submitted.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getContext(), "You can only review a product once.", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ReviewResponse> call, Throwable t) {
                                et_review.setEnabled(true);
                                img_send.setEnabled(true);
                                Toast.makeText(getContext(), "An error occurred while submitting the review.", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            });
        } else {
            mLayoutManager = new LinearLayoutManager(getContext());
            rv_reviews.setLayoutManager(mLayoutManager);
            mAdapter = new ProductReviewsAdapter(getContext(), reviews);
            rv_reviews.setAdapter(mAdapter);
            mAdapter.notifyItemRangeChanged(0, reviews.size());
        }


        return v;
    }
}
