package com.zoogtech.onlineshopping.modules.cart.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.zoogtech.onlineshopping.BaseActivity;
import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.CartItem;
import com.zoogtech.onlineshopping.models.Shop;
import com.zoogtech.onlineshopping.modules.cart.CartFragment;

import java.util.ArrayList;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rnecesito on 3/18/16.
 */
public class CartShopAdapter extends RecyclerView.Adapter<CartShopAdapter.ViewHolder> {
    Context context;
    ArrayList<Shop> shops;
    Map<String, ArrayList<CartItem>> itemsPerShop;

    public CartShopAdapter(Context context, ArrayList<Shop> shops, Map<String, ArrayList<CartItem>> itemsPerShop) {
        this.context = context;
        this.shops = shops;
        this.itemsPerShop = itemsPerShop;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cart_shop, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Shop item = shops.get(position);
        final ArrayList<CartItem> items = itemsPerShop.get(item.getUserId());
        holder.label_shop_name.setText(item.getUser().getFirstName() + (item.getUser().getLastName() != null ? " " + item.getUser().getLastName() : "") + "\'s Shop");
        holder.label_items_purchased.setText("Items Purchased: " + items.size());
        Glide.with(context).load(item.getUser().getProfilePhoto().getSecure_url()).fitCenter().into(holder.img_shop_owner);
        float totalPrice = 0;
        for (CartItem c : items) {
            totalPrice += (c.getQuantity() * c.getProductId().getPrice());
        }
        holder.label_items_total.setText("₱" + String.format("%.02f", totalPrice));
        holder.shop_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnlineShopping.setSelectedShop(item);
                CartFragment fragment = new CartFragment();
                if (context == null) {
                    return;
                } else {
                    Bundle b = new Bundle();
                    b.putSerializable("shopProducts", items);
                    fragment.setArguments(b);
                    BaseActivity mainActivity = (BaseActivity) context;
//                    mainActivity.switchContent(fragment, "cart");
                    mainActivity.switchContent(fragment);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return shops.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.label_shop_name)
        TextView label_shop_name;
        @Bind(R.id.label_items_purchased)
        TextView label_items_purchased;
        @Bind(R.id.label_items_total)
        TextView label_items_total;
        @Bind(R.id.img_shop_owner)
        ImageView img_shop_owner;
        @Bind(R.id.shop_container)
        MaterialRippleLayout shop_container;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
