package com.zoogtech.onlineshopping.modules.login.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zoogtech.onlineshopping.modules.login.LoginFragment;
import com.zoogtech.onlineshopping.modules.login.RegisterFragment;

/**
 * Created by rnecesito on 3/16/16.
 */
public class LoginPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    LoginFragment lf;
    RegisterFragment rf;
    private String tabTitles[] = new String[] { "LOGIN", "REGISTER" };
    private FragmentManager fm;
    private Context context;

    public LoginPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.fm = fm;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (lf == null) {
                    lf = new LoginFragment();
                }
                return lf;
            case 1:
                if (rf == null) {
                    rf = new RegisterFragment();
                }
                return rf;
        }
        return null;
    }


    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
