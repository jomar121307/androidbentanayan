package com.zoogtech.onlineshopping.modules.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.modules.login.adapters.LoginPagerAdapter;
import com.zoogtech.onlineshopping.utils.LoadingFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rnecesito on 3/16/16.
 */
public class LoginMainFragment extends Fragment {
    private static String TAG = "LOGIN";
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    @Bind(R.id.sliding_tabs)
    TabLayout tablayout;

    private CallbackManager callbackManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getContext().getApplicationContext());
        View v = inflater.inflate(R.layout.fragment_login_main, container, false);
        ButterKnife.bind(this, v);

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Bundle bundle = new Bundle();
                String token = loginResult.getAccessToken().getToken();
                bundle.putString("type", "fb");
                bundle.putString("token", token);
                LoadingFragment fragment = new LoadingFragment();
                fragment.setArguments(bundle);
                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, fragment);
                fragmentTransaction.commit();
            }

            @Override
            public void onCancel() {
                Log.i("FB CB", "Login Cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("FB CB", "Login Failed");
                Log.i("FB CB", error.getLocalizedMessage());
            }
        });
        viewpager.setAdapter(new LoginPagerAdapter(getChildFragmentManager(), getContext()));
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Login");
                } else {
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Register");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tablayout.setupWithViewPager(viewpager);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle b = getArguments();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Login");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
