package com.zoogtech.onlineshopping.modules.home.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.zoogtech.onlineshopping.BaseActivity;
import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Subcategory;
import com.zoogtech.onlineshopping.modules.products.ProductListFragment;
import com.zoogtech.onlineshopping.utils.RecyclingPagerAdapter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.trinea.android.common.util.ListUtils;

/**
 * Created by rnecesito on 3/17/16.
 */
public class SubcategAdapter extends RecyclingPagerAdapter {
    private Context context;
    private ArrayList<Subcategory> items;

    private int size;
    private boolean isInfiniteLoop;

    public SubcategAdapter(Context context, ArrayList<Subcategory> items) {
        this.context = context;
        this.items = items;
        this.size = ListUtils.getSize(items);
        isInfiniteLoop = false;
    }

    @Override
    public int getCount() {
        return isInfiniteLoop ? Integer.MAX_VALUE : ListUtils.getSize(items);
    }

    private int getPosition(int position) {
        return isInfiniteLoop ? position % size : position;
    }


    @Override
    public View getView(int position, View view, ViewGroup container) {
        final Subcategory item = items.get(position);
        view = LayoutInflater.from(container.getContext()).inflate(R.layout.item_category2, container, false);
        ViewHolder holder = new ViewHolder(view, container);
        holder.label_category.setText(item.getGroupheader().getName());
        Glide.with(context).load(R.drawable.ph_categ).into(holder.img_category);
        holder.subcateg_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductListFragment fragment = new ProductListFragment();
                if (context == null)
                    return;
                if (context instanceof BaseActivity) {
                    Bundle b = new Bundle();
                    b.putString("slug", OnlineShopping.getCategorySlug() + "." + item.getGroupheader().getSlug());
                    b.putString("subcategory", item.getGroupheader().getName());
                    fragment.setArguments(b);
                    BaseActivity mainActivity = (BaseActivity) context;
//                    mainActivity.switchContent(fragment, "productlist");
                    mainActivity.switchContent(fragment);
                }
            }
        });
        return view;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.img_category)
        ImageView img_category;
        @Bind(R.id.label_category)
        TextView label_category;
        @Bind(R.id.subcateg_container)
        RelativeLayout subcateg_container;

        public ViewHolder(View itemView, ViewGroup parent) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * @return the isInfiniteLoop
     */
    public boolean isInfiniteLoop() {
        return isInfiniteLoop;
    }

    /**
     * @param isInfiniteLoop the isInfiniteLoop to set
     */
    public SubcategAdapter setInfiniteLoop(boolean isInfiniteLoop) {
        this.isInfiniteLoop = isInfiniteLoop;
        return this;
    }
}
