package com.zoogtech.onlineshopping.modules.payment.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Address;
import com.zoogtech.onlineshopping.models.ResetPassResponse;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.models.Shipping_2;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 3/23/16.
 */
public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Shipping_2> addresses;
    private String addressId;

    public AddressAdapter(Context context, ArrayList<Shipping_2> addresses) {
        this.context = context;
        this.addresses = addresses;
        this.addressId = "";
    }

    public String getAddressId() {
        return addressId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_address, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Address address = addresses.get(position).getAddress();
        holder.label_shipping_address.setText("Shipping Address " + (position + 1));
        holder.label_address.setText(address.getFullName() + " " + address.getStreetAddress() + " " + address.getCity() + " " + address.getProvince() + " " + address.getCountry() + " " + address.getZipcode());
        holder.rb_selected_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressId = addresses.get(position).getId();
            }
        });
        holder.address_container.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context, 0);
                builder.setTitle("Remove shipping address");
                builder.setMessage("Are you sure you want to remove this shipping address? (You cannot undo this action)");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final ProgressDialog mDialog = new ProgressDialog(context, ProgressDialog.STYLE_SPINNER);
                        mDialog.setIndeterminate(true);
                        mDialog.setMessage("Removing shipping address...");
                        mDialog.show();
                        Session session = OnlineShopping.getSession();
                        ApiService apiService = new RestClient().getApiService();
                        Call<ResetPassResponse> call = apiService.removeAddress(
                                "Bearer " + session.getAccessToken(),
                                addresses.get(position).getId()
                        );
                        call.enqueue(new Callback<ResetPassResponse>() {
                            @Override
                            public void onResponse(Call<ResetPassResponse> call, Response<ResetPassResponse> response) {
                                mDialog.dismiss();
                                if (response.isSuccess()) {
                                    addresses.remove(position);
                                    notifyItemRemoved(position);
                                } else {
                                    Toast.makeText(context, "An error occurred while removing the shipping address.", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResetPassResponse> call, Throwable t) {
                                mDialog.dismiss();
                                Toast.makeText(context, "An error occurred while removing the shipping address.", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return addresses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.label_shipping_address)
        TextView label_shipping_address;
        @Bind(R.id.label_address)
        TextView label_address;
        @Bind(R.id.rb_selected_address)
        RadioButton rb_selected_address;
        @Bind(R.id.address_container)
        LinearLayout address_container;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
