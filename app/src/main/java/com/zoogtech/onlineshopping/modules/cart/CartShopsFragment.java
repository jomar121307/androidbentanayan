package com.zoogtech.onlineshopping.modules.cart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.CartItem;
import com.zoogtech.onlineshopping.models.CartResponse;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.models.Shop;
import com.zoogtech.onlineshopping.modules.cart.adapters.CartShopAdapter;
import com.zoogtech.onlineshopping.modules.home.HomeFragment;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;
import com.zoogtech.onlineshopping.utils.Cache;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 3/18/16.
 */
public class CartShopsFragment extends Fragment {
    private static String TAG = "CART";
    @Bind(R.id.rv_cart)
    RecyclerView rv_cart;
    @Bind(R.id.loader_layout)
    RelativeLayout loader_layout;
    @Bind(R.id.empty_cart_layout)
    CardView empty_cart_layout;
    @Bind(R.id.btn_action)
    Button btn_action;
    @Bind(R.id.main_message)
    TextView main_message;
    @Bind(R.id.sub_message)
    TextView sub_message;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cart_shops, container, false);
        ButterKnife.bind(this, v);

        loader_layout.setVisibility(View.VISIBLE);
        ApiService apiService = new RestClient().getApiService();
        Session token = null;
        try {
            token = (Session) Cache.readObject(getContext(), "session");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        Call<CartResponse> call = apiService.getCart("Bearer " + token.getAccessToken());
        call.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                loader_layout.setVisibility(View.GONE);
                if (response.isSuccess()) {
                    Log.i(TAG, response.body().getItems().size() + "");
                    if (response.body().getItems().size() > 0) {
                        mLayoutManager = new LinearLayoutManager(getContext());
                        rv_cart.setLayoutManager(mLayoutManager);
                        ArrayList<Shop> shops = new ArrayList<Shop>();
                        Map<String,ArrayList<CartItem>> itemsPerShop = new HashMap<>();
                        for (CartItem c : response.body().getItems()) {
                            if(!itemsPerShop.containsKey(c.getShopId().getUserId())){
                                itemsPerShop.put(c.getShopId().getUserId(), new ArrayList<CartItem>());
                                Shop s = c.getShopId();
                                s.setUser(c.getProductId().getUserId());
                                shops.add(s);
                            }
                            itemsPerShop.get(c.getShopId().getUserId()).add(c);
                        }
                        mAdapter = new CartShopAdapter(getContext(), shops, itemsPerShop);
                        rv_cart.setAdapter(mAdapter);
                        mAdapter.notifyItemRangeChanged(0, shops.size());
                    } else {
                        empty_cart_layout.setVisibility(View.VISIBLE);
                        btn_action.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                HomeFragment hf = new HomeFragment();
                                android.support.v4.app.FragmentTransaction ft;
                                getActivity().getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                ft = getActivity().getSupportFragmentManager().beginTransaction();
                                ft.remove(CartShopsFragment.this);
                                ft.replace(R.id.frame, hf);
                                ft.commit();
                            }
                        });
                    }
                } else {
                    Log.i(TAG, response.message());
                }
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {
                loader_layout.setVisibility(View.GONE);
                empty_cart_layout.setVisibility(View.VISIBLE);
                main_message.setText("An error occured!");
                sub_message.setText("There was an error when accessing the server.");
                btn_action.setText("Try Again");
                btn_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        android.support.v4.app.FragmentTransaction ft;
                        CartShopsFragment hf = new CartShopsFragment();
                        ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame, hf);
                        ft.commit();
                    }
                });
            }
        });


        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Cart - Select Shop");
    }
}
