package com.zoogtech.onlineshopping.modules.payment;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.modules.home.HomeFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rnecesito on 3/29/16.
 */
public class PayPalFragment extends Fragment {
    @Bind(R.id.wv_paypal)
    WebView wv_paypal;
    String pp_url = "";

    @SuppressLint("SetJavaScriptEnabled")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_paypal, container, false);
        ButterKnife.bind(this, v);

        pp_url = getArguments().getString("url");
        assert pp_url != null;
        if (!pp_url.equals("")) {

            wv_paypal.getSettings().setJavaScriptEnabled(true);
            wv_paypal.getSettings().setDomStorageEnabled(true);
            wv_paypal.getSettings().setLoadWithOverviewMode(true);
            wv_paypal.getSettings().setUseWideViewPort(true);
            wv_paypal.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    if (url.contains("/#/myshop/purchases")) {
                        HomeFragment hf = new HomeFragment();
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frame, hf);
                        ft.commit();
                        Toast.makeText(getContext(), "Payment sucessful.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//                    handler.proceed();
//                    error.getCertificate();
                }
            });
            wv_paypal.loadUrl(pp_url);
        }

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("PayPal");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }
}
