package com.zoogtech.onlineshopping.modules.shop;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.models.Shop;
import com.zoogtech.onlineshopping.models.User;
import com.zoogtech.onlineshopping.modules.shop.adapters.ShopPagerAdapter;
import com.zoogtech.onlineshopping.utils.Cache;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rnecesito on 3/30/16.
 */
public class ShopMainFragment extends Fragment {
    private static String TAG = "SHOP";
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    @Bind(R.id.sliding_tabs)
    TabLayout tablayout;
    User user;
    User me;
    Shop shop;
    String shopId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login_main, container, false);

        ButterKnife.bind(this, v);

        if (getArguments() != null) {
            user = (User) getArguments().getSerializable("user");
            shop = (Shop) getArguments().getSerializable("shop");
            shopId = shop.getId();
        } else {
            Session session = null;
            try {
                session = (Session) Cache.readObject(getContext(), "session");
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            if (session != null) {
                user = session.getUserId();
                shopId = session.getShopId();
            }
        }
        OnlineShopping.setShopUser(user);

        viewpager.setAdapter(new ShopPagerAdapter(getChildFragmentManager(), getActivity(), user, shopId));
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                if (position == 0) {
//                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Shop Page");
//                } else {
//                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Profile");
//                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tablayout.setupWithViewPager(viewpager);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if (getArguments() != null) {
            user = (User) getArguments().getSerializable("user");
            shop = (Shop) getArguments().getSerializable("shop");
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(user.getFirstName() + "\'s Shop");
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        } else {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("My Shop");
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }
}
