package com.zoogtech.onlineshopping.modules.products;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Product;
import com.zoogtech.onlineshopping.models.ProductsResponse;
import com.zoogtech.onlineshopping.modules.products.adapters.ProductListAdapter;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 3/17/16.
 */
public class ProductListFragment extends Fragment{
    private static String TAG = "PRODUCTS";
    @Bind(R.id.rv_products)
    RecyclerView rv_products;
    @Bind(R.id.loader_layout)
    RelativeLayout loader_layout;
    @Bind(R.id.empty_cart_layout)
    CardView empty_cart_layout;
    @Bind(R.id.btn_action)
    Button btn_action;
    @Bind(R.id.main_message)
    TextView main_message;
    @Bind(R.id.sub_message)
    TextView sub_message;

    private RecyclerView.LayoutManager mLayoutManager;
    private ProductListAdapter mAdapter;
    private ArrayList<Product> products;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_productlist, container, false);
        ButterKnife.bind(this, v);
        if (products == null) {
            Bundle b = getArguments();
            String slug = b.getString("slug");
            Log.i("SLUG", "where={\"category\":{\"startsWith\" : \"" + slug + "\"}}");
            loader_layout.setVisibility(View.VISIBLE);
            ApiService apiService = new RestClient().getApiService();
            Call<ProductsResponse> call  = apiService.getProductsFromSlug("{\"category\":{\"startsWith\" : \"" + slug + "\"}}");
            call.enqueue(new Callback<ProductsResponse>() {
                @Override
                public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                    loader_layout.setVisibility(View.GONE);
                    if (response.isSuccess()) {
                        if (response.body().getCount() > 0) {
                            mLayoutManager = new GridLayoutManager(getContext(), 2);
                            rv_products.setLayoutManager(mLayoutManager);
                            products = response.body().getProducts();
                            mAdapter = new ProductListAdapter(getActivity(), products);
                            rv_products.setAdapter(mAdapter);
                            mAdapter.notifyItemRangeChanged(0, products.size());
                        } else if (response.body().getCount() == 0){
                            empty_cart_layout.setVisibility(View.VISIBLE);
                            main_message.setText("There are no products available");
                            btn_action.setText("Tap to go back");
                            btn_action.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    getFragmentManager().popBackStack();
                                }
                            });
                        } else {
                            empty_cart_layout.setVisibility(View.VISIBLE);
                            main_message.setText("There are no products available");
                            btn_action.setText("Tap to go back");
                            btn_action.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    getFragmentManager().popBackStack();
                                }
                            });
                        }
                    } else {
                        main_message.setText("An error occured!");
                        sub_message.setText("There was an error while accessing the server.");
                        btn_action.setText("Tap to go back");
                        btn_action.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getFragmentManager().popBackStack();
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<ProductsResponse> call, Throwable t) {
                    main_message.setText("An error occured!");
                    sub_message.setText("There was an error when accessing the server.");
                    btn_action.setText("Tap to go back");
                    btn_action.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getFragmentManager().popBackStack();
                        }
                    });
                }
            });
        } else {
            mLayoutManager = new GridLayoutManager(getContext(), 2);
            rv_products.setLayoutManager(mLayoutManager);
            mAdapter = new ProductListAdapter(getContext(), products);
            rv_products.setAdapter(mAdapter);
            mAdapter.notifyItemRangeChanged(0, products.size());
        }

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);
        Bundle b = getArguments();
        if (b.getString("subcategory") != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(b.getString("subcategory"));
        }
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                getActivity().onBackPressed();
//                return true;
//
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

}
