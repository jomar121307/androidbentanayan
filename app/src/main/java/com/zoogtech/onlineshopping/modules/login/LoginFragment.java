package com.zoogtech.onlineshopping.modules.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.facebook.login.widget.LoginButton;
import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.LoginResponse;
import com.zoogtech.onlineshopping.models.ResetPassResponse;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.modules.home.HomeFragment;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;
import com.zoogtech.onlineshopping.utils.Cache;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 3/15/16.
 */
public class LoginFragment extends Fragment {
    @Bind(R.id.usernameWrapper)
    TextInputLayout usernameWrapper;
    @Bind(R.id.passwordWrapper) TextInputLayout passwordWrapper;
    @Bind(R.id.login_btn)
    ActionProcessButton login_btn;
    @Bind(R.id.login_ups_btn)
    ActionProcessButton login_ups_btn;
    @Bind(R.id.real_login_fb)
    LoginButton real_login_fb;

    private ProgressDialog mDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, v);

        usernameWrapper.setHint("Username");
        passwordWrapper.setHint("Password");

        List<String> permissionNeeds = Arrays.asList("user_photos", "email", "user_birthday", "public_profile");
        real_login_fb.setReadPermissions(permissionNeeds);
        real_login_fb.setFragment(getParentFragment());

        return v;
    }

    private void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @OnClick(R.id.login_btn)
    public void login(View v) {
        hideKeyboard();

        String username = usernameWrapper.getEditText().getText().toString();
        String password = passwordWrapper.getEditText().getText().toString();
        if (!validateEmail(username)) {
            usernameWrapper.setError("Enter your username!");
        } else if (!validatePassword(password)) {
            passwordWrapper.setError("Enter your password!");
        } else {
            usernameWrapper.setErrorEnabled(false);
            passwordWrapper.setErrorEnabled(false);
            doLogin();
        }
    }

    @OnClick(R.id.login_ups_btn)
    public void login_ups(View v) {
        hideKeyboard();

        String username = usernameWrapper.getEditText().getText().toString();
        String password = passwordWrapper.getEditText().getText().toString();
        if (!validateEmail(username)) {
            usernameWrapper.setError("Enter your username!");
        } else if (!validatePassword(password)) {
            passwordWrapper.setError("Enter your password!");
        } else {
            usernameWrapper.setErrorEnabled(false);
            passwordWrapper.setErrorEnabled(false);
            doLoginUps();
        }
    }

    @OnClick(R.id.tv_forgot_pass)
    public void forgot_pass(View v) {
        hideKeyboard();
        final AlertDialog.Builder inputAlert = new AlertDialog.Builder(getContext());
        inputAlert.setTitle("Forgot Password");
        final EditText email = new EditText(getContext());
        email.setHint("Email");
        email.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        inputAlert.setView(email, 50, 0, 50, 0);
        inputAlert.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String emailString = email.getText().toString();
                if (email.equals("")) {
                    Toast.makeText(getContext(), "Please enter your email address.", Toast.LENGTH_SHORT).show();
                } else {
                    mDialog = new ProgressDialog(getContext(), ProgressDialog.STYLE_SPINNER);
                    mDialog.setIndeterminate(true);
                    mDialog.setMessage("Selecting address..");
                    mDialog.setCancelable(false);
                    mDialog.show();
                    ApiService apiService = new RestClient().getApiService();
                    Call<ResetPassResponse> call = apiService.resetPass(
                            emailString
                    );
                    call.enqueue(new Callback<ResetPassResponse>() {
                        @Override
                        public void onResponse(Call<ResetPassResponse> call, Response<ResetPassResponse> response) {
                            mDialog.dismiss();
                            if (response.isSuccess()) {
                                if (response.message().equals("success")) {
                                    Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResetPassResponse> call, Throwable t) {
                            mDialog.dismiss();
                            Toast.makeText(getContext(), "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
        inputAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = inputAlert.create();
        alertDialog.show();
    }

    public boolean validateEmail(String email) {
//        matcher = pattern.matcher(email);
//        return matcher.matches();
        return email.length() > 0;
    }

    public boolean validatePassword(String password) {
        return password.length() > 0;
    }

    public void doLogin() {
        String username = usernameWrapper.getEditText().getText().toString();
        String password = passwordWrapper.getEditText().getText().toString();
        login_btn.setMode(ActionProcessButton.Mode.ENDLESS);
        login_btn.setProgress(1);
        ApiService service = new RestClient().getApiService();
        Call<LoginResponse> call = service.login(username, password, "native");
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                switch (response.code()) {
                    case 403:
                        Log.i("Response", response.raw().message());
                        usernameWrapper.setError("Invalid credentials.");
                        passwordWrapper.setError("Invalid credentials.");
                        login_btn.setText("Press to login again.");
                        login_btn.setProgress(-1);
                        break;
                    case 200:
                        Session s = response.body().getSession();
                        s.setShopId(response.body().getShop());
                        OnlineShopping.setSession(s);
                        OnlineShopping.setUser(s.getUserId());
                        try {
                            Cache.writeObject(getContext(), "session", s);
                            HomeFragment fragment = new HomeFragment();
                            android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.frame, fragment);
                            fragmentTransaction.commit();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    default:
                        Log.i("Response", response.message());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                login_btn.setText("Login failed. Press to login again.");
                login_btn.setProgress(-1);
            }
        });
    }

    public void doLoginUps() {
        String username = usernameWrapper.getEditText().getText().toString();
        String password = passwordWrapper.getEditText().getText().toString();
        login_ups_btn.setMode(ActionProcessButton.Mode.ENDLESS);
        login_ups_btn.setProgress(1);
        ApiService service = new RestClient().getApiService();
        Call<LoginResponse> call = service.login(username, password, "native");
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                switch (response.code()) {
                    case 403:
                        Log.i("Response", response.raw().message());
                        usernameWrapper.setError("Invalid credentials.");
                        passwordWrapper.setError("Invalid credentials.");
                        login_ups_btn.setText("Press to login again.");
                        login_ups_btn.setProgress(-1);
                        break;
                    case 200:
                        Session s = response.body().getSession();
                        s.setShopId(response.body().getShop());
                        OnlineShopping.setSession(s);
                        OnlineShopping.setUser(s.getUserId());
                        try {
                            Cache.writeObject(getContext(), "session", s);
                            HomeFragment fragment = new HomeFragment();
                            android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.frame, fragment);
                            fragmentTransaction.commit();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    default:
                        Log.i("Response", response.message());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                login_ups_btn.setText("Login failed. Press to login again.");
                login_ups_btn.setProgress(-1);
            }
        });
    }

    @OnClick(R.id.login_fb)
    void login_with_fb(View v) {
        Log.i("LOGIN WITH FB", "TRUE");
        real_login_fb.performClick();
    }

}
