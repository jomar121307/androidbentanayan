package com.zoogtech.onlineshopping.modules.home.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.zoogtech.onlineshopping.BaseActivity;
import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Subcategory;
import com.zoogtech.onlineshopping.modules.products.ProductListFragment;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rnecesito on 3/16/16.
 */
public class SubsubcategoryAdapter extends RecyclerView.Adapter<SubsubcategoryAdapter.ViewHolder> {
    Context context;
    ArrayList<Subcategory.Group> categories;
    String cslug;
    String scslug;

    public SubsubcategoryAdapter(Context context, ArrayList<Subcategory.Group> categories, String cslug, String scslug) {
        this.context = context;
        this.categories = categories;
        this.cslug = cslug;
        this.scslug = scslug;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_subcategory, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Subcategory.Group item = categories.get(position);
        holder.label_category.setText(item.getName());
        Glide.with(context).load(R.drawable.ph_categ).into(holder.img_category);
        holder.categ_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductListFragment fragment = new ProductListFragment();
                if (context == null)
                    return;
                if (context instanceof BaseActivity) {
                    Bundle b = new Bundle();
                    b.putString("subcategory", item.getName());
                    b.putString("slug", OnlineShopping.getCategorySlug() + "." + OnlineShopping.getSubcategorySlug() + "." + item.getSlug());
                    fragment.setArguments(b);
                    BaseActivity mainActivity = (BaseActivity) context;
//                    mainActivity.switchContent(fragment, "productlist");
                    mainActivity.switchContent(fragment);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.img_category)
        ImageView img_category;
        @Bind(R.id.label_category)
        TextView label_category;
        @Bind(R.id.categ_container)
        MaterialRippleLayout categ_container;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
