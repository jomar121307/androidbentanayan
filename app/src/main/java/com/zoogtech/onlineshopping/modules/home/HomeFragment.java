package com.zoogtech.onlineshopping.modules.home;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.zoogtech.onlineshopping.BaseFragment;
import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Category;
import com.zoogtech.onlineshopping.models.CategoryResponse;
import com.zoogtech.onlineshopping.models.Subcategory;
import com.zoogtech.onlineshopping.modules.home.adapters.CategoryAdapter;
import com.zoogtech.onlineshopping.modules.home.adapters.SubcategAdapter;
import com.zoogtech.onlineshopping.modules.home.adapters.SubsubcategoryAdapter;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 3/16/16.
 */
public class HomeFragment extends BaseFragment {
    private static String TAG = "HOME";
    @Bind(R.id.categs)
    AutoScrollViewPager categs;
    @Bind(R.id.subcategs)
    AutoScrollViewPager subcategs;
    @Bind(R.id.rv_categs_2)
    RecyclerView rv2;
    @Bind(R.id.loader_layout)
    RelativeLayout loader_layout;
    @Bind(R.id.empty_cart_layout)
    CardView empty_cart_layout;
    @Bind(R.id.btn_action)
    Button btn_action;
    @Bind(R.id.main_message)
    TextView main_message;
    @Bind(R.id.sub_message)
    TextView sub_message;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private String cslug;
    private String scslug;
    private ArrayList<Category> c;
    private ArrayList<Subcategory> sc;
    private ArrayList<Subcategory.Group> ssc;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, v);

        if (OnlineShopping.getSession() != null) {
            Log.i("INFO", new Gson().toJson(OnlineShopping.getSession()));
        } else {
            Log.i("INFO", "NO USER");
        }

        if (c == null) {
            loader_layout.setVisibility(View.VISIBLE);
            ApiService apiService = new RestClient().getApiService();
            Call<ArrayList<CategoryResponse>> call  = apiService.getCategories();
            call.enqueue(new Callback<ArrayList<CategoryResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<CategoryResponse>> call, final Response<ArrayList<CategoryResponse>> response) {
                    if (response.isSuccess()) {
                        c = response.body().get(0).getMetadata();
                        loader_layout.setVisibility(View.GONE);
                        categs.setAdapter(new CategoryAdapter(getContext(), c).setInfiniteLoop(false));
                        categs.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                            }

                            @Override
                            public void onPageSelected(int position) {
                                sc = c.get(position).getSubcategories();
                                cslug = c.get(position).getSlug();
                                OnlineShopping.setCategorySlug(cslug);
                                Log.i("CSLUG", cslug);
                                if (sc.size() > 0) {
                                    subcategs.setAdapter(new SubcategAdapter(getContext(), sc).setInfiniteLoop(false));
                                    subcategs.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                        @Override
                                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                                        }

                                        @Override
                                        public void onPageSelected(int position2) {
                                            rv2.scrollToPosition(0);
                                            ssc = sc.get(position2).getGroupling();
                                            scslug = sc.get(position2).getGroupheader().getSlug();
                                            Log.i("SCSLUG", scslug);
                                            OnlineShopping.setSubcategorySlug(scslug);
                                            if (ssc.size() > 0) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        rv2.removeAllViews();
                                                        rv2.swapAdapter(new SubsubcategoryAdapter(getContext(), ssc, cslug, scslug), false);
                                                    }
                                                }, 200);
                                            } else {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        rv2.removeAllViews();
                                                        rv2.swapAdapter(new SubsubcategoryAdapter(getContext(), new ArrayList<Subcategory.Group>(), cslug, scslug), false);
                                                    }
                                                }, 200);
                                            }
                                        }

                                        @Override
                                        public void onPageScrollStateChanged(int state) {

                                        }
                                    });
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            rv2.removeAllViews();
                                            rv2.swapAdapter(new SubsubcategoryAdapter(getContext(), sc.get(0).getGroupling(), cslug, scslug), false);
                                            rv2.scrollToPosition(0);
                                        }
                                    }, 200);
                                } else {
                                    subcategs.setAdapter(new SubcategAdapter(getContext(), new ArrayList<Subcategory>()));
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            rv2.removeAllViews();
                                            rv2.swapAdapter(new SubsubcategoryAdapter(getContext(), new ArrayList<Subcategory.Group>(), cslug, scslug), false);
                                        }
                                    }, 200);
                                }
                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });
                        sc = c.get(0).getSubcategories();
                        subcategs.setAdapter(new SubcategAdapter(getContext(), c.get(0).getSubcategories()).setInfiniteLoop(false));
                        subcategs.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                            }

                            @Override
                            public void onPageSelected(int position2) {
                                rv2.scrollToPosition(0);
                                ssc = sc.get(position2).getGroupling();
                                scslug = sc.get(position2).getGroupheader().getSlug();
                                Log.i("SCSLUG", scslug);
                                OnlineShopping.setSubcategorySlug(scslug);
                                if (ssc.size() > 0) {
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            rv2.removeAllViews();
                                            rv2.swapAdapter(new SubsubcategoryAdapter(getContext(), ssc, cslug, scslug), false);
                                        }
                                    }, 200);
                                } else {
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            rv2.removeAllViews();
                                            rv2.swapAdapter(new SubsubcategoryAdapter(getContext(), new ArrayList<Subcategory.Group>(), cslug, scslug), false);
                                        }
                                    }, 200);
                                }
                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });
                        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                        rv2.setLayoutManager(mLayoutManager);
                        mAdapter = new SubsubcategoryAdapter(getContext(), c.get(0).getSubcategories().get(0).getGroupling(), cslug, scslug);
                        rv2.setAdapter(mAdapter);
                        mAdapter.notifyItemRangeChanged(0, c.get(0).getSubcategories().get(0).getGroupling().size());
                        rv2.scrollToPosition(0);
                        cslug = c.get(0).getSlug();
                        OnlineShopping.setCategorySlug(cslug);
                        scslug = c.get(0).getSubcategories().get(0).getGroupheader().getSlug();
                        OnlineShopping.setSubcategorySlug(scslug);
                    } else {
                        Log.i(TAG, response.message());
                        empty_cart_layout.setVisibility(View.VISIBLE);
                        main_message.setText("No Categories");
                        sub_message.setText("There are no categories available.");
                        btn_action.setVisibility(View.INVISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<CategoryResponse>> call, Throwable t) {
                    loader_layout.setVisibility(View.GONE);
                    empty_cart_layout.setVisibility(View.VISIBLE);
                    main_message.setText("An error occured!");
                    sub_message.setText("There was an error while accessing the server.");
                    btn_action.setText("Try Again");
                    btn_action.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            android.support.v4.app.FragmentTransaction ft;
                            HomeFragment f = new HomeFragment();
                            ft = getActivity().getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.frame, f);
                            ft.commit();
                        }
                    });
                }
            });
        } else {
            loader_layout.setVisibility(View.GONE);
            categs.setAdapter(new CategoryAdapter(getContext(), c).setInfiniteLoop(false));
            categs.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    sc = c.get(position).getSubcategories();
                    cslug = c.get(position).getSlug();
                    OnlineShopping.setCategorySlug(cslug);
                    if (sc.size() > 0) {
                        subcategs.setAdapter(new SubcategAdapter(getContext(), sc).setInfiniteLoop(false));
                        subcategs.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                            }

                            @Override
                            public void onPageSelected(int position2) {
                                rv2.scrollToPosition(0);
                                ssc = sc.get(position2).getGroupling();
                                scslug = sc.get(position2).getGroupheader().getSlug();
                                OnlineShopping.setSubcategorySlug(scslug);
                                if (ssc.size() > 0) {
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            rv2.removeAllViews();
                                            rv2.swapAdapter(new SubsubcategoryAdapter(getContext(), ssc, cslug, scslug), false);
                                        }
                                    }, 200);
                                } else {
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            rv2.removeAllViews();
                                            rv2.swapAdapter(new SubsubcategoryAdapter(getContext(), new ArrayList<Subcategory.Group>(), cslug, scslug), false);
                                        }
                                    }, 200);
                                }
                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                rv2.removeAllViews();
                                rv2.swapAdapter(new SubsubcategoryAdapter(getContext(), sc.get(0).getGroupling(), cslug, scslug), false);
                                rv2.scrollToPosition(0);
                            }
                        }, 200);
                    } else {
                        subcategs.setAdapter(new SubcategAdapter(getContext(), new ArrayList<Subcategory>()));
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                rv2.removeAllViews();
                                rv2.swapAdapter(new SubsubcategoryAdapter(getContext(), new ArrayList<Subcategory.Group>(), cslug, scslug), false);
                            }
                        }, 200);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            subcategs.setAdapter(new SubcategAdapter(getContext(), c.get(0).getSubcategories()).setInfiniteLoop(false));
            mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
            rv2.setLayoutManager(mLayoutManager);
            mAdapter = new SubsubcategoryAdapter(getContext(), c.get(0).getSubcategories().get(0).getGroupling(), cslug, scslug);
            rv2.setAdapter(mAdapter);
            mAdapter.notifyItemRangeChanged(0, c.get(0).getSubcategories().get(0).getGroupling().size());
            rv2.scrollToPosition(0);
            cslug = c.get(0).getSlug();
            OnlineShopping.setCategorySlug(cslug);
            scslug = c.get(0).getSubcategories().get(0).getGroupheader().getSlug();
            OnlineShopping.setSubcategorySlug(scslug);
        }

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Home");
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
