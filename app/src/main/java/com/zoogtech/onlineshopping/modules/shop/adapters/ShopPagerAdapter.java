package com.zoogtech.onlineshopping.modules.shop.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zoogtech.onlineshopping.OnlineShopping;
import com.zoogtech.onlineshopping.models.User;
import com.zoogtech.onlineshopping.modules.shop.ShopFragment;
import com.zoogtech.onlineshopping.modules.shop.UserFragment;

/**
 * Created by rnecesito on 3/30/16.
 */
public class ShopPagerAdapter  extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] { "SHOP PAGE", "PROFILE" };
    private FragmentManager fm;
    private Context context;
    private User user;
    private String shopId;

    public ShopPagerAdapter(FragmentManager fm, Context context, User user, String shopId) {
        super(fm);
        this.fm = fm;
        this.context = context;
        this.user = user;
        this.shopId = shopId;
        OnlineShopping.setShopUser(user);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ShopFragment();
            case 1:
                return new UserFragment();
        }
        return null;
    }


    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
