package com.zoogtech.onlineshopping.modules.history;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zoogtech.onlineshopping.R;
import com.zoogtech.onlineshopping.models.Order;
import com.zoogtech.onlineshopping.models.OrdersResponse;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.modules.history.adapters.HistoryOrdersAdapter;
import com.zoogtech.onlineshopping.modules.home.HomeFragment;
import com.zoogtech.onlineshopping.requests.ApiService;
import com.zoogtech.onlineshopping.requests.RestClient;
import com.zoogtech.onlineshopping.utils.Cache;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rnecesito on 3/29/16.
 */
public class PurchaseHistoryOrdersFragment extends Fragment {
    @Bind(R.id.rv_history)
    RecyclerView rv_history;
    @Bind(R.id.loader_layout)
    RelativeLayout loader_layout;
    @Bind(R.id.empty_cart_layout)
    CardView empty_cart_layout;
    @Bind(R.id.btn_action)
    Button btn_action;
    @Bind(R.id.main_message)
    TextView main_message;
    @Bind(R.id.sub_message)
    TextView sub_message;
    @Bind(R.id.ll_order_info)
    LinearLayout ll_order_info;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<Order> orders;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_purchase_history, container, false);
        ButterKnife.bind(this, v);
        ll_order_info.setVisibility(View.GONE);
        rv_history.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1.0f));
        loader_layout.setVisibility(View.VISIBLE);
        ApiService apiService = new RestClient().getApiService();
        Session token = null;
        try {
            token = (Session) Cache.readObject(getContext(), "session");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        Call<OrdersResponse> call = apiService.getPurchaseHistory("Bearer " + token.getAccessToken());
        call.enqueue(new Callback<OrdersResponse>() {
            @Override
            public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                loader_layout.setVisibility(View.GONE);
                if (response.isSuccess()) {
                    if (response.body().getCount() > 0) {
                        mLayoutManager = new LinearLayoutManager(getContext());
                        rv_history.setLayoutManager(mLayoutManager);
                        mAdapter = new HistoryOrdersAdapter(getContext(), response.body().getOrders());
                        rv_history.setAdapter(mAdapter);
                        mAdapter.notifyItemRangeChanged(0, response.body().getCount());
                    } else {
                        empty_cart_layout.setVisibility(View.VISIBLE);
                        main_message.setText("You haven't made any purchases!");
                        btn_action.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                android.support.v4.app.FragmentTransaction ft;
                                HomeFragment f = new HomeFragment();
                                ft = getActivity().getSupportFragmentManager().beginTransaction();
                                ft.replace(R.id.frame, f);
                                ft.commit();
                            }
                        });
                    }
                } else {
                    Log.i("HIST", response.message());
                    loader_layout.setVisibility(View.GONE);
                    empty_cart_layout.setVisibility(View.VISIBLE);
                    main_message.setText("An error occured!");
                    sub_message.setText("There was an error while accessing the server.");
                    btn_action.setText("Try Again");
                    btn_action.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            android.support.v4.app.FragmentTransaction ft;
                            PurchaseHistoryOrdersFragment f = new PurchaseHistoryOrdersFragment();
                            ft = getActivity().getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.frame, f);
                            ft.commit();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<OrdersResponse> call, Throwable t) {
                Log.i("HIST", t.getMessage());
                loader_layout.setVisibility(View.GONE);
                empty_cart_layout.setVisibility(View.VISIBLE);
                main_message.setText("An error occured!");
                sub_message.setText("There was an error while accessing the server.");
                btn_action.setText("Try Again");
                btn_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        android.support.v4.app.FragmentTransaction ft;
                        PurchaseHistoryOrdersFragment f = new PurchaseHistoryOrdersFragment();
                        ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame, f);
                        ft.commit();
                    }
                });
            }
        });


        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Purchased Items");
    }
}
