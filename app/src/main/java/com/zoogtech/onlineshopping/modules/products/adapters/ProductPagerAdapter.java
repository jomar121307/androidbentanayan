package com.zoogtech.onlineshopping.modules.products.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zoogtech.onlineshopping.modules.products.ProductDetailsFragment;
import com.zoogtech.onlineshopping.modules.products.ProductReviewsFragment;

/**
 * Created by rnecesito on 4/4/16.
 */
public class ProductPagerAdapter extends FragmentPagerAdapter {


    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] { "Product Details", "Product Reviews" };
    private FragmentManager fm;
    ProductDetailsFragment pdf;
    ProductReviewsFragment prf;

    public ProductPagerAdapter(FragmentManager fm) {
        super(fm);
        this.fm = fm;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (pdf == null) {
                    pdf = new ProductDetailsFragment();
                }
                return pdf;
            case 1:
                if (prf == null) {
                    prf = new ProductReviewsFragment();
                }
                return prf;
        }
        return null;
    }


    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
