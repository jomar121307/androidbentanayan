package com.zoogtech.onlineshopping.requests;

import com.zoogtech.onlineshopping.models.AddAddress;
import com.zoogtech.onlineshopping.models.CartAddResponse;
import com.zoogtech.onlineshopping.models.CartAddResponse_2;
import com.zoogtech.onlineshopping.models.CartResponse;
import com.zoogtech.onlineshopping.models.CategoryResponse;
import com.zoogtech.onlineshopping.models.CheckoutInfoResponse;
import com.zoogtech.onlineshopping.models.LoginResponse;
import com.zoogtech.onlineshopping.models.Message;
import com.zoogtech.onlineshopping.models.OrdersResponse;
import com.zoogtech.onlineshopping.models.PayResponse;
import com.zoogtech.onlineshopping.models.PayUPSResponse;
import com.zoogtech.onlineshopping.models.PrivateMessage;
import com.zoogtech.onlineshopping.models.Product;
import com.zoogtech.onlineshopping.models.ProductsResponse;
import com.zoogtech.onlineshopping.models.ResetPassResponse;
import com.zoogtech.onlineshopping.models.ReviewResponse;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.models.Shipping_2;
import com.zoogtech.onlineshopping.models.User;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Rey on 1/26/2016.
 */
public interface ApiService {
    String HEADER = "X-App : zcommerce";
    String HEADER2 = "Content-Type : application/json";

    @Headers(HEADER)
    @FormUrlEncoded
    @POST("sessions")
    Call<LoginResponse> loginWithFb(
            @Field("accessToken") String accessToken,
            @Field("socialNetwork") String socialNetwork
    );

    @Headers(HEADER)
    @FormUrlEncoded
    @POST("sessions")
    Call<LoginResponse> login(
            @Field("userEmail") String username,
            @Field("password") String password,
            @Field("type") String type
    );

    //Refresh Session
    @Headers(HEADER)
    @FormUrlEncoded
    @POST("sessions")
    Call<Session> refreshSession(
            @Field("refreshToken") String refreshToken,
            @Field("accessToken") String accessToken
    );

    //Request reset password
    @Headers(HEADER)
    @FormUrlEncoded
    @POST("users/resetpassword")
    Call<ResetPassResponse> resetPass(
            @Field("email") String email
    );

    //Register new user
    @Headers(HEADER)
    @FormUrlEncoded
    @POST("users")
    Call<User> createUser(
            @Field("email") String email,
            @Field("username") String username,
            @Field("password") String password,
            @Field("confirmPassword") String confirmPassword,
            @Field("firstName") String firstName,
            @Field("lastName") String lastName,
            @Field("gender") String gender
    );

    //Update user
    @Headers(HEADER)
    @FormUrlEncoded
    @PUT("users")
    Call<User> updateUser(
            @Header("Authorization") String accessToken,
            @Query("userId") String userId,
            @FieldMap Map<String, String> data
    );

    //Get Categories
    @Headers(HEADER)
    @GET("misc?name=Product+Categories")
    Call<ArrayList<CategoryResponse>> getCategories();

    //Get Products
    @Headers(HEADER)
    @GET("products")
    Call<ProductsResponse> getProducts();

    //Get Products from slug
    @Headers(HEADER)
    @GET("products")
    Call<ProductsResponse> getProductsFromSlug(
            @Query("where") String slug
    );

    @Headers(HEADER)
    @GET("products/{id}?reviews=true")
    Call<Product> getSingleProduct(
            @Path("id") String productId
    );

    //Get products from user
    @Headers(HEADER)
    @GET("products")
    Call<ProductsResponse> getUserProducts(
//            @Header("Authorization") String token,
            @Query("where") String where
    );

    //Get Cart items
    @Headers(HEADER)
    @GET("cart")
    Call<CartResponse> getCart(
            @Header("Authorization") String token
    );

    //Add item to Cart
    @Headers(HEADER)
    @FormUrlEncoded
    @POST("cart")
    Call<CartAddResponse> addToCart(
            @Header("Authorization") String token,
            @Field("productId") String productId,
            @Field("quantity") int quantity
    );

    //Update cart item
    @Headers(HEADER)
    @FormUrlEncoded
    @POST("cart/{id}")
    Call<CartAddResponse_2> updateCartItem(
            @Header("Authorization") String token,
            @Path("id") String productId,
            @Field("quantity") int quantity
    );

    //Remove item from Cart
    @Headers(HEADER)
    @DELETE("cart/{id}")
    Call<CartAddResponse> removeFromCart(
            @Header("Authorization") String token,
            @Path("id") String cartId
    );

    //Get shipping addresses
    @Headers(HEADER)
    @GET("shipping")
    Call<ArrayList<Shipping_2>> getAddresses(
            @Header("Authorization") String token,
            @Query("limit") String limit
    );

    //Add shipping address
    @Headers({HEADER, HEADER2})
    @POST("shipping")
    Call<Shipping_2> addAddress(
            @Header("Authorization") String token,
            @Body AddAddress map
    );

    //Remove shipping address
    @Headers(HEADER)
    @DELETE("shipping/{id}")
    Call<ResetPassResponse> removeAddress(
            @Header("Authorization") String token,
            @Path("id") String addressId
    );

    //Select shipping address
    @Headers(HEADER)
    @FormUrlEncoded
    @POST("setcheckoutinfo")
    Call<CheckoutInfoResponse> setCheckoutInfo(
            @Header("Authorization") String token,
            @Field("checkoutShopId") String checkoutShopId,
            @Field("shippingId") String shippingId,
            @Field("paymentPlatform") String platform
    );

    //Pay for cart with PayPal
    @Headers(HEADER)
    @GET("gonnapaynow")
    Call<PayResponse> pay(
      @Header("Authorization") String token
    );

    //Pay for cart with UPS
    @Headers(HEADER)
    @FormUrlEncoded
    @POST("gonnapaynow")
    Call<PayUPSResponse> payWithUPS(
            @Header("Authorization") String token,
            @Field("username") String username,
            @Field("password") String password
    );

    //Get purchase history
    @Headers(HEADER)
    @GET("orders?purchases=true&sort=createdAt+DESC")
    Call<OrdersResponse> getPurchaseHistory(
            @Header("Authorization") String token
    );

    //Get message threads/rooms
    @Headers(HEADER)
    @GET("rooms")
    Call<ArrayList<PrivateMessage>> getMessages(
            @Header("Authorization") String token,
            @Query("sort") String sort
    );

    //Get messages
    @Headers(HEADER)
    @GET("rooms/{id}")
    Call<ArrayList<Message>> getRoomMessages(
            @Header("Authorization") String token,
            @Path("id") String roomId
    );

    //Post message
    @Headers(HEADER)
    @FormUrlEncoded
    @POST("message/{id}")
    Call<Message> postMessage(
            @Header("Authorization") String token,
            @Path("id") String userId,
            @Field("roomId") String roomId,
            @Field("payload") JSONObject payload
    );

    //Initiate message
    @Headers(HEADER)
    @FormUrlEncoded
    @POST("messages/initiate/{id}")
    Call<Message> startMessage(
            @Header("Authorization") String token,
            @Path("id") String userId,
            @Field("payload") JSONObject payload
    );

    //Post review of product
    @Headers(HEADER)
    @FormUrlEncoded
    @POST("reviews")
    Call<ReviewResponse> postReview(
            @Header("Authorization") String token,
            @Field("productId") String productId,
            @Field("userId") String userId,
            @Field("content") String content,
            @Field("appType") String appType,
            @Field("rating") float rating
    );

}
