package com.zoogtech.onlineshopping.requests;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rey on 1/26/2016.
 */
public class RestClient {
    private static final String BASE_URL = "https://api.bentanayan.com";
//    private static final String BASE_URL = "http://192.168.0.105";
    private ApiService apiService;

    public RestClient() {
        Gson gson = new GsonBuilder().create();

        Retrofit restAdapter = new Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiService = restAdapter.create(ApiService.class);
    }

    public ApiService getApiService() {
        return apiService;
    }

}