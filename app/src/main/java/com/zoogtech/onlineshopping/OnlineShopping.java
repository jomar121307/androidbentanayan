package com.zoogtech.onlineshopping;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.zoogtech.onlineshopping.models.Page_2;
import com.zoogtech.onlineshopping.models.Product;
import com.zoogtech.onlineshopping.models.Session;
import com.zoogtech.onlineshopping.models.Shop;
import com.zoogtech.onlineshopping.models.User;
import com.zoogtech.onlineshopping.utils.Cache;

import io.fabric.sdk.android.Fabric;
import java.io.IOException;

/**
 * Created by Rey on 1/26/2016.
 */
public class OnlineShopping extends Application {
    private static OnlineShopping appInstance;
    private static User user;
    private static Session session;
    private static Page_2 chosenPage;
    private static String checkIn;
    private static String checkOut;
    private static int adults;
    private static int children;
    private static int price;
    private static boolean forSomeone;
    private static User shopUser;
    private static User checkoutShopUser;
    private static Shop selectedShop;
    private static Product selectedProduct;
    private static String categorySlug;
    private static String subcategorySlug;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        appInstance = this;
        setForSomeone(false);
        FacebookSdk.sdkInitialize(appInstance);
        try {
            if (Cache.readObject(this, "session") != null) {
                session = (Session) Cache.readObject(this, "session");
                user = session.getUserId();
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static synchronized OnlineShopping getAppInstance() {
        return appInstance;
    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        OnlineShopping.user = user;
    }

    public static Session getSession() {
        return session;
    }

    public static void setSession(Session session) {
        OnlineShopping.session = session;
    }

    public static void clearData() {
        user = null;
        session = null;
    }

    public static Page_2 getChosenPage() {
        return chosenPage;
    }

    public static void setChosenPage(Page_2 chosenPage) {
        OnlineShopping.chosenPage = chosenPage;
    }

    public static String getCheckIn() {
        return checkIn;
    }

    public static void setCheckIn(String checkIn) {
        OnlineShopping.checkIn = checkIn;
    }

    public static String getCheckOut() {
        return checkOut;
    }

    public static void setCheckOut(String checkOut) {
        OnlineShopping.checkOut = checkOut;
    }

    public static int getAdults() {
        return adults;
    }

    public static void setAdults(int adults) {
        OnlineShopping.adults = adults;
    }

    public static int getChildren() {
        return children;
    }

    public static void setChildren(int children) {
        OnlineShopping.children = children;
    }

    public static int getPrice() {
        return price;
    }

    public static void setPrice(int price) {
        OnlineShopping.price = price;
    }

    public static boolean isForSomeone() {
        return forSomeone;
    }

    public static void setForSomeone(boolean forSomeone) {
        OnlineShopping.forSomeone = forSomeone;
    }

    public static User getShopUser() {
        return shopUser;
    }

    public static void setShopUser(User shopUser) {
        OnlineShopping.shopUser = shopUser;
    }

    public static Product getSelectedProduct() {
        return selectedProduct;
    }

    public static void setSelectedProduct(Product selectedProduct) {
        OnlineShopping.selectedProduct = selectedProduct;
    }

    public static String getCategorySlug() {
        return categorySlug;
    }

    public static void setCategorySlug(String categorySlug) {
        OnlineShopping.categorySlug = categorySlug;
    }

    public static String getSubcategorySlug() {
        return subcategorySlug;
    }

    public static void setSubcategorySlug(String subcategorySlug) {
        OnlineShopping.subcategorySlug = subcategorySlug;
    }

    public static User getCheckoutShopUser() {
        return checkoutShopUser;
    }

    public static void setCheckoutShopUser(User checkoutShopUser) {
        OnlineShopping.checkoutShopUser = checkoutShopUser;
    }

    public static Shop getSelectedShop() {
        return selectedShop;
    }

    public static void setSelectedShop(Shop selectedShop) {
        OnlineShopping.selectedShop = selectedShop;
    }
}
