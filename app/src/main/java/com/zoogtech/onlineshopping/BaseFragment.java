package com.zoogtech.onlineshopping;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import com.zoogtech.onlineshopping.utils.Cache;

import java.io.IOException;

/**
 * Created by rnecesito on 3/18/16.
 */
public class BaseFragment extends Fragment {

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        NavigationView navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(6).setVisible(false);
        navigationView.getMenu().getItem(7).setVisible(false);
        if (isLoggedIn()) {
            navigationView.getMenu().getItem(0).setVisible(false);
            navigationView.getMenu().getItem(2).setVisible(true);
            navigationView.getMenu().getItem(3).setVisible(true);
            navigationView.getMenu().getItem(4).setVisible(true);
            navigationView.getMenu().getItem(5).setVisible(true);
            navigationView.getMenu().getItem(8).setVisible(true);
        } else {
            navigationView.getMenu().getItem(0).setVisible(true);
            navigationView.getMenu().getItem(2).setVisible(false);
            navigationView.getMenu().getItem(3).setVisible(false);
            navigationView.getMenu().getItem(4).setVisible(false);
            navigationView.getMenu().getItem(5).setVisible(false);
            navigationView.getMenu().getItem(8).setVisible(false);
        }
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        String title = toolbar.getTitle().toString();
        switch (title) {
            case "Login":
            case "Register":
                navigationView.getMenu().getItem(0).setChecked(true);
                break;
            case "Home":
                navigationView.getMenu().getItem(1).setChecked(true);
                break;
            case "My Shop":
                navigationView.getMenu().getItem(2).setChecked(true);
                break;
            case "Cart - Select Shop":
                navigationView.getMenu().getItem(3).setChecked(true);
                break;
            case "Purchased Items":
                navigationView.getMenu().getItem(4).setChecked(true);
                break;
            case "Messages":
                navigationView.getMenu().getItem(5).setChecked(true);
                break;
        }

    }

    public boolean isLoggedIn() {
        boolean toReturn = false;
        try {
            toReturn = Cache.readObject(getContext(), "session") != null;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return toReturn;
    }
}
